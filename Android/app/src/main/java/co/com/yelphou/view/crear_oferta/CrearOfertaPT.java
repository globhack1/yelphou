package co.com.yelphou.view.crear_oferta;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Spinner;

import com.google.gson.Gson;

import java.util.List;

import co.com.yelphou.R;
import co.com.yelphou.data.OnGetDatabase;
import co.com.yelphou.data.db.User;
import co.com.yelphou.data.remote.OfertaService;
import co.com.yelphou.data.user.UserRepository;
import co.com.yelphou.model.Localidad;
import co.com.yelphou.model.LoggedInUser;
import co.com.yelphou.model.Oferta;
import co.com.yelphou.view.crear_oferta.data.OfertaRepository;
import co.com.yelphou.view.login.LoginActivity;
import co.com.yelphou.view.register.util.SpinnerLocalidadAdapter;

public class CrearOfertaPT implements CrearOfertaCT.presenter {

    private Context context;
    private CrearOfertaCT.view view;

    private int categoria = 0;

    private OfertaRepository repository;
    private UserRepository userRepository;

    LoggedInUser user;


    public CrearOfertaPT(Context context, CrearOfertaCT.view view) {
        this.context = context;
        this.view = view;

        repository = OfertaRepository.getInstance();
        userRepository = new UserRepository(context);
        getUser();
    }

    private void getUser() {
        userRepository.getUser(1L, new OnGetDatabase<User>() {
            @Override
            public void onGet(User obj) {
                if (obj != null) {
                    String da = new Gson().toJson(obj.data);
                    da = da.substring(0, da.length() - 1);
                    da = da.substring(1);
                    da = da.replace("\\\"", "\"");


                    user = new Gson().fromJson(da, LoggedInUser.class);
                    view.setNombreUsuario(user.getNombres()+" "+user.getApellidos());
                }
            }
        });
    }

    @Override
    public void clickImagen(int pos) {
        categoria = pos;

        view.quitarTodoBackground();
        view.ponerBackground(pos);

    }

    @Override
    public void clickGuardar(String descripcion, int localidad) {
        view.hideKeyboard();

        if(categoria == 0){
            view.mostrarToast("Por favor, seleccione una categoria");
            return;
        }

        if(descripcion.equals("")){
            view.mostrarToast("Por favor, escriba una descripción");
            return;
        }

        Oferta oferta = new Oferta();
        oferta.categoriaAyudaId = categoria;
        oferta.descripcion = descripcion;
        oferta.localidadId = localidades.get(localidad).id;
        oferta.usuarioOferenteId = user.getId();
        oferta.gps = "4.649062, -74.101602";

        String data = new Gson().toJson(oferta);
        Log.e("data", "registro:--- "+data );

        repository.crearOferta(oferta, obj -> {
            if(obj != null){
                view.limpiarCampos();
                view.mostrarAgradecimiento();
            } else {
                view.mostrarToast("Ocurrio un error, por favor, verifica tu conexión a internet");
            }
        });

    }

    Spinner spin_localidad;
    private SpinnerLocalidadAdapter adapterLoc;
    private List<Localidad> localidades;
    @Override
    public void setAdapterLocalidad(Spinner localidad) {
        spin_localidad = localidad;
        repository.obtenerLocalidades(obj -> {
            if(obj != null){
                localidades = obj;
                Localidad[] array = new Localidad[obj.size()];
                obj.toArray(array);

                adapterLoc = new SpinnerLocalidadAdapter(context,
                        R.layout.spinner_item,
                        array);

                spin_localidad.setAdapter(adapterLoc);

            }else {
                view.mostrarToast("Ocurrio un error, por favor, revisa tu conexión a internet");
            }
        });
    }
}
