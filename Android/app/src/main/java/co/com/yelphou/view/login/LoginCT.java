package co.com.yelphou.view.login;

import androidx.lifecycle.LiveData;

interface LoginCT {

    interface view {

    }

    interface presenter {
        LiveData<LoginFormState> getLoginFormState();
        LiveData<LoginResult>  getLoginResult();
        void loginDataChanged(String username, String password);
        void login(String username, String password);
    }
}
