package co.com.yelphou.view.perfilUsuario.ofertante;

import androidx.annotation.StringRes;

import java.util.List;

import co.com.yelphou.model.AyudaDonante;
import co.com.yelphou.model.LoggedInUser;

public interface PerfilUsuarioCT {

    interface view {

        void setUserInfo(LoggedInUser object);
        void showError(Integer errorString);

        void setAyudas(List<AyudaDonante> object);

        void logout();
    }

    interface presenter {

        void getInfoUsuario();

        void logout();
    }
}
