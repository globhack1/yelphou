package co.com.yelphou.data.remote;

public interface CallbackResponse<T> {
    void onResponse(T object);
    void onError(String msg);
}
