package co.com.yelphou.view.verOfertas.data;

import android.util.Log;

import com.google.gson.Gson;

import java.util.List;

import co.com.yelphou.RetrofitClientInstance;
import co.com.yelphou.data.OnGetDatabase;
import co.com.yelphou.data.remote.AyudasService;
import co.com.yelphou.model.AyudaActiva;
import co.com.yelphou.model.TipoDocumento;
import co.com.yelphou.view.verOfertas.AplicarAyuda;
import co.com.yelphou.view.verOfertas.ResponseAplicar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class AyudasRepository {


    Retrofit retrofit;
    AyudasService service;


    private static AyudasRepository instance;

    public static AyudasRepository getInstance(){
        if(instance == null){
            instance = new AyudasRepository();
        }
        return instance;
    }

    private AyudasRepository() {
        this.retrofit = RetrofitClientInstance.getRetrofitInstance();
        service = retrofit.create(AyudasService.class);
    }


    public void obtenerAyudasActivas(int localidad, int categoria, OnGetDatabase<List<AyudaActiva>> callback){
        service.getAyudasReceptor(localidad, categoria).enqueue(new Callback<List<AyudaActiva>>() {
            @Override
            public void onResponse(Call<List<AyudaActiva>> call, Response<List<AyudaActiva>> response) {
                Log.e("data", "onResponse: " );
                callback.onGet(response.body());
            }

            @Override
            public void onFailure(Call<List<AyudaActiva>> call, Throwable t) {
                Log.e("data", "onFailure: " );
                callback.onGet(null);
            }
        });
    }


    public void aplicar(AplicarAyuda aplicarAyuda, OnGetDatabase<ResponseAplicar> callback){
        service.aplicarAyuda(aplicarAyuda).enqueue(new Callback<ResponseAplicar>() {
            @Override
            public void onResponse(Call<ResponseAplicar> call, Response<ResponseAplicar> response) {
                callback.onGet(response.body());
                Log.e("data", "onResponse: " );
                String data = new Gson().toJson(response.body());
                Log.e("data", "aplicarAyuda:  "+data );

            }

            @Override
            public void onFailure(Call<ResponseAplicar> call, Throwable t) {
                callback.onGet(null);
                Log.e("data", "onFailure: " );
            }
        });
    }


}
