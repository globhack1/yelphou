package co.com.yelphou.view.verOfertas;

import android.content.Context;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import java.util.List;

import co.com.yelphou.data.OnGetDatabase;
import co.com.yelphou.data.db.User;
import co.com.yelphou.data.user.UserRepository;
import co.com.yelphou.model.AyudaActiva;
import co.com.yelphou.model.LoggedInUser;
import co.com.yelphou.view.verOfertas.adapter.OfertasAdapter;
import co.com.yelphou.view.verOfertas.data.AyudasRepository;
import co.com.yelphou.view.verOfertas.util.SolicitarOferta;

public class VerOfertasPT implements VerOfertasCT.presenter, SolicitarOferta {

    private VerOfertasCT.view view;
    private Context context;
    private UserRepository userRepository;
    private LoggedInUser user;
    private AyudasRepository ayudasRepository;
    private OfertasAdapter ofertasAdapter;
    private List<AyudaActiva> ayudaActivas;
    private boolean settedAdapter = false;
    private boolean dataGetted = false;



    public VerOfertasPT(VerOfertasCT.view view, Context context) {
        this.view = view;
        this.context = context;

        ayudasRepository = AyudasRepository.getInstance();

        userRepository = new UserRepository(context);
        getUser();

    }

    private void getAyudasActivas(int categoria) {
        Log.e("Data", "getAyudasActivas: "+user.getLocalidad().id );
        ayudasRepository.obtenerAyudasActivas(user.getLocalidad().id, categoria, new OnGetDatabase<List<AyudaActiva>>() {
            @Override
            public void onGet(List<AyudaActiva> obj) {
                ayudaActivas = obj;
                dataGetted = true;
                setDataRecycler();
            }
        });
    }


    private void setDataRecycler(){
        Log.e("prese", "setDataRecycler: " );
        if(dataGetted && settedAdapter){
            Log.e("prese", "setDataRecycler: "+ayudaActivas.size() );
            ofertasAdapter.setList(ayudaActivas);
            ofertasAdapter.notifyDataSetChanged();
        }
    }


    private void getUser() {
        userRepository.getUser(1L, new OnGetDatabase<User>() {
            @Override
            public void onGet(User obj) {
                if (obj != null) {
                    String da = new Gson().toJson(obj.data);
                    da = da.substring(0, da.length() - 1);
                    da = da.substring(1);
                    da = da.replace("\\\"", "\"");


                    user = new Gson().fromJson(da, LoggedInUser.class);
                    view.setNombreUsuario(user.getNombres()+" "+user.getApellidos());
                    getAyudasActivas(-1);
                } else {

                }
            }
        });
    }

    @Override
    public void cambiaCategoria(int pos) {
        getAyudasActivas(pos);
    }

    @Override
    public void setAdapterCategoria(Spinner categoria) {

        String[] catagorias = new String[7];
        catagorias[0] = "Elige el tipo de ayuda que buscas";
        catagorias[1] = "Alimentos";
        catagorias[2] = "Ropa";
        catagorias[3] = "Hacer Tramites";
        catagorias[4] = "Acompañamiento";
        catagorias[5] = "Reciclaje";
        catagorias[6] = "Otros";


        ArrayAdapter<String> a =new ArrayAdapter<>(context,android.R.layout.simple_spinner_item, catagorias);
        a.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        categoria.setAdapter(a);

    }


    @Override
    public void setAdapterOfertas(RecyclerView ofertas) {

        ofertasAdapter = new OfertasAdapter(context, ayudaActivas, this);

        ofertas.setLayoutManager(new LinearLayoutManager(context));

        ofertas.setAdapter(ofertasAdapter);

        ofertas.setNestedScrollingEnabled(false);

        settedAdapter = true;
        setDataRecycler();

    }

    @Override
    public void solicitar(int pos, String descripcion) {
        Log.e("Ver", "solicitar: "+pos);
        Log.e("Ver", "solicitar: "+descripcion);

        AplicarAyuda aplicarAyuda = new AplicarAyuda();

        aplicarAyuda.ayudaId = ayudaActivas.get(pos).id;
        aplicarAyuda.descripcion = descripcion;
        aplicarAyuda.usuarioReceptor = user.getId();

        String data = new Gson().toJson(aplicarAyuda);
        Log.e("Ver", "solicitar:  "+data );

        ayudasRepository.aplicar(aplicarAyuda, new OnGetDatabase<ResponseAplicar>() {
            @Override
            public void onGet(ResponseAplicar obj) {
                if(obj!=null) {
                    ayudaActivas.get(pos).estrella = true;
                    ofertasAdapter.notifyDataSetChanged();
                } else {

                }
            }
        });

    }
}
