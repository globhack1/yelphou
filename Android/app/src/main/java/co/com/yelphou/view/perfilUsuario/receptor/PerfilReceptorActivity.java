package co.com.yelphou.view.perfilUsuario.receptor;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.com.yelphou.R;
import co.com.yelphou.model.AyudaDonante;
import co.com.yelphou.model.LoggedInUser;
import co.com.yelphou.model.SolicitudAyuda;
import co.com.yelphou.view.login.LoginActivity;
import co.com.yelphou.view.perfilUsuario.receptor.adapter.HistorialAdapter;

public class PerfilReceptorActivity extends AppCompatActivity implements PerfilUsuarioCT.view{

    private PerfilUsuarioCT.presenter presenter;

    @BindView(R.id.tvDireccion)
    TextView direccion;
    @BindView(R.id.tvFechaNacimiento)
    TextView fechaNacimiento;
    @BindView(R.id.tvLocalidad)
    TextView localidad;
    @BindView(R.id.tvNombre)
    TextView nombre;
    @BindView(R.id.tvNumeroDocumento)
    TextView numeroDocumento;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @OnClick(R.id.ivLogout)
    public void onLogout(){
        presenter.logout();
    }

    private HistorialAdapter historialAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil_usuario);
        ButterKnife.bind(this);
        presenter = new PerfilUsuarioPT(this, this);
        presenter.getInfoUsuario();
        initRecycler();
    }

    private void initRecycler(){
        historialAdapter = new HistorialAdapter(mOnClickListener);
        recyclerView.setAdapter(historialAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(itemDecoration);
    }

    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
           /* int itemPosition = recyclerView.getChildLayoutPosition(view);
            AyudaDonante ayudaDonante = historialAdapter.getAyudas().get(itemPosition);

            Intent intent = new Intent(PerfilReceptorActivity.this, DetallePerfilUsuarioActivity.class);
            intent.putExtra(DetallePerfilUsuarioActivity.AYUDA, new Gson().toJson(ayudaDonante));
            startActivity(intent);*/
        }
    };

    @Override
    public void setUserInfo(LoggedInUser object) {
        direccion.setText(object.getDireccion());
        fechaNacimiento.setText(object.getFechaNacimiento());
        localidad.setText(object.getLocalidad().nombre);
        nombre.setText(object.getNombres()+" " +object.getApellidos());
        numeroDocumento.setText(object.getTipoDocumento().codigo + " "+ object.getDocumento());
    }

    public void showError(@StringRes Integer errorString) {
        Toast.makeText(getApplicationContext(), errorString, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setAyudas(List<SolicitudAyuda> object) {
        historialAdapter.setData(object);
    }

    @Override
    public void logout() {
        Intent i = new Intent(this, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        this.finish();
    }
}
