package co.com.yelphou.view.register;

import android.content.Context;

import android.util.Log;
import android.widget.Spinner;

import com.google.gson.Gson;

import java.util.List;

import co.com.yelphou.R;

import co.com.yelphou.model.Localidad;
import co.com.yelphou.model.TipoDocumento;
import co.com.yelphou.model.TipoUsuario;
import co.com.yelphou.model.User;
import co.com.yelphou.view.register.data.RegistroRepository;
import co.com.yelphou.view.register.util.SpinnerLocalidadAdapter;
import co.com.yelphou.view.register.util.SpinnerTipoDocAdapter;
import co.com.yelphou.view.register.util.SpinnerTipoUsuarioAdapter;


public class RegisterPT implements RegisterCT.presenter {

    private RegisterCT.view view;
    private Context context;
    private  RegistroRepository registroRepository;


    public RegisterPT(RegisterCT.view view, Context context) {
        this.view = view;
        this.context = context;
        this.registroRepository =  RegistroRepository.getInstance();
    }

    Spinner spin_tipo_doc;
    boolean tipo_docGet;
    private SpinnerTipoDocAdapter adapter;
    private List<TipoDocumento> tiposDoc;
    @Override
    public void setAdaterTipo_Doc(final Spinner tipo_doc) {
        spin_tipo_doc = tipo_doc;

        registroRepository.obtenerTipoDocumento(obj -> {
            if(obj != null){
                tiposDoc = obj;

                TipoDocumento[] array = new TipoDocumento[obj.size()];
                obj.toArray(array);

                adapter = new SpinnerTipoDocAdapter(context,
                        R.layout.spinner_item,
                        array);

                spin_tipo_doc.setAdapter(adapter);
                tipo_docGet = true;
                quitarLoading();

            } else {
                view.mostrarAlerta("Ocurrio un error, por favor, revisa tu conexión a internet");
            }
        });

    }

    Spinner spin_localidad;
    boolean localidadGet;
    private SpinnerLocalidadAdapter adapterLoc;
    private List<Localidad> localidades;
    @Override
    public void setAdapterLocalidad(Spinner localidad) {
        spin_localidad = localidad;
        registroRepository.obtenerLocalidades(obj -> {
            if(obj != null){
                localidades = obj;
                Localidad[] array = new Localidad[obj.size()];
                obj.toArray(array);

                adapterLoc = new SpinnerLocalidadAdapter(context,
                        R.layout.spinner_item,
                        array);

                spin_localidad.setAdapter(adapterLoc);
                localidadGet = true;
                quitarLoading();

            }else {
                view.mostrarAlerta("Ocurrio un error, por favor, revisa tu conexión a internet");
            }
        });
    }

    Spinner spin_tipo_usua;
    boolean tipo_usuaGet;
    private SpinnerTipoUsuarioAdapter adapterTipoUs;
    private List<TipoUsuario> tipoUsuarios;
    @Override
    public void setAdapterTipoUsuario(Spinner tipo_usua) {
        spin_tipo_usua = tipo_usua;
        registroRepository.obtenerTipoUsuario(obj -> {
            if(obj != null){
                tipoUsuarios = obj;
                TipoUsuario[] array = new TipoUsuario[obj.size()];
                obj.toArray(array);

                adapterTipoUs = new SpinnerTipoUsuarioAdapter(context,
                        R.layout.spinner_item,
                        array);

                spin_tipo_usua.setAdapter(adapterTipoUs);
                tipo_usuaGet = true;
                quitarLoading();

            }else {
                view.mostrarAlerta("Ocurrio un error, por favor, revisa tu conexión a internet");
            }
        });
    }

    private void quitarLoading(){
        if(tipo_docGet && localidadGet && tipo_usuaGet){
            view.ocultarLoading();
        }
    }

    @Override
    public void registro(String correo, String contra, String nombre, String apellido, int tipo_doc, String numero_doc, String fecha_nac, String direccion, int localidad, int tipo_user) {
        view.mostrarLoading();
        User user = new User();

        user.correo = correo;
        user.contrasena = contra;
        user.nombres = nombre;
        user.apellidos = apellido;
        user.tipoDocumentoId = tiposDoc.get(tipo_doc).id;
        Log.e("data", "registro: TipoDoc "+tiposDoc.get(tipo_doc).id );
        if(numero_doc!=null && !numero_doc.equals("")){
            user.documento = Long.parseLong(numero_doc);
        }
        user.fechaNacimiento = fecha_nac;
        user.direccion = direccion;
        user.localidadId = localidades.get(localidad).id;
        Log.e("Data", "registro: Locali "+localidades.get(localidad).id );
        user.tipoUsuarioId = tipoUsuarios.get(tipo_user).id;
        Log.e("Data", "registro: Tipo Usuari "+tipoUsuarios.get(tipo_user).id );

        String data = new Gson().toJson(user);
        Log.e("data", "registro:  "+data );

        RegistroRepository.getInstance().registro(user, obj -> {
            if(obj!=null){
                view.mostrarAlerta("Usuario guardado correctamente");
            } else {

                view.mostrarToast("Ocurrio un error");
            }
            view.ocultarLoading();

        });

    }
}
