package co.com.yelphou.view.perfilUsuario.ofertante;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.com.yelphou.R;
import co.com.yelphou.model.AyudaDonante;
import co.com.yelphou.model.LoggedInUser;
import co.com.yelphou.model.SolicitudAyuda;
import co.com.yelphou.view.perfilUsuario.ofertante.adapter.AceptarRechazarCallback;
import co.com.yelphou.view.perfilUsuario.ofertante.adapter.AyudaSolicitudAdapter;

public class DetallePerfilUsuarioActivity extends AppCompatActivity  implements DetallePerfilUsuarioCT.view{

    public static final String AYUDA = "param_ayuda";
    private DetallePerfilUsuarioCT.presenter presenter;
    private AyudaSolicitudAdapter adapter;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.ivCategoria)
    ImageView imageView;
    @BindView(R.id.tvCategoria)
    TextView categoria;
    @BindView(R.id.tvNombre)
    TextView nombre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_perfil_usuario);
        ButterKnife.bind(this);

        Intent i =  getIntent();

        AyudaDonante ayudaDonante = null;
        if(i.hasExtra(AYUDA)){
            ayudaDonante = new Gson().fromJson(i.getStringExtra(AYUDA), AyudaDonante.class);
        }

        int imagen = R.drawable.icono_acompanamientos_perfil;
        switch (ayudaDonante.getCategoriaAyuda().getId()){
            case 4: imagen = R.drawable.icono_acompanamientos_perfil; break;
            case 1: imagen = R.drawable.icono_alimentos_perfil; break;
            case 6: imagen = R.drawable.icono_otros_perfil; break;
            case 5: imagen = R.drawable.icono_reciclaje_perfil; break;
            case 2: imagen = R.drawable.icono_ropa_perfil; break;
            case 3: imagen = R.drawable.icono_tramites_perfil; break;
        }
        imageView.setImageResource(imagen);
        categoria.setText(ayudaDonante.getCategoriaAyuda().getDescripcion());

        presenter = new DetallePerfilUsuarioPT(this, ayudaDonante, this);
        initRecycler();

        presenter.obtenerInteresados();
    }

    private void initRecycler(){
        adapter = new AyudaSolicitudAdapter(onAceptarRechazar);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(itemDecoration);
    }

    AceptarRechazarCallback onAceptarRechazar = new AceptarRechazarCallback() {
        @Override
        public void aceptar(SolicitudAyuda sa) {
            presenter.aceptarSolicitud(sa);
        }

        @Override
        public void rechazar(SolicitudAyuda sa) {
            presenter.rechazarSolicitud(sa);
        }
    };

    @Override
    public void showError(Integer errorString) {
        Toast.makeText(getApplicationContext(), errorString, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setSolicitudes(List<SolicitudAyuda> solicitudes) {
        adapter.setData(solicitudes);
    }

    @Override
    public void setInfoUsuario(LoggedInUser object) {
        nombre.setText(object.getNombres()+" " +object.getApellidos());
    }
}
