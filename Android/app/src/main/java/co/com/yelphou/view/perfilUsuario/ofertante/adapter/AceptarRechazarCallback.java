package co.com.yelphou.view.perfilUsuario.ofertante.adapter;

import co.com.yelphou.model.SolicitudAyuda;

public interface AceptarRechazarCallback {
    void aceptar(SolicitudAyuda sa);
    void rechazar(SolicitudAyuda sa);
}
