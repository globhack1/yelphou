package co.com.yelphou.data.user;


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import co.com.yelphou.data.db.User;

@Dao
public interface UserDAO {


    @Insert
    Long insertUser(User user);

    @Update
    int updateUser(User user);

    @Delete
    void deleteUser(User user);


    @Query("SELECT * FROM user WHERE id=:id")
    User getUser(Long id);

}
