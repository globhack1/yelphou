package co.com.yelphou.view.perfilUsuario.ofertante;

import android.content.Context;

import java.util.List;

import co.com.yelphou.R;
import co.com.yelphou.data.remote.CallbackResponse;
import co.com.yelphou.data.repository.AyudasRepository;
import co.com.yelphou.model.AyudaDonante;
import co.com.yelphou.model.EEstados;
import co.com.yelphou.model.LoggedInUser;
import co.com.yelphou.model.SolicitudAyuda;
import co.com.yelphou.view.login.data.LoginDataSource;
import co.com.yelphou.view.login.data.LoginRepository;

public class DetallePerfilUsuarioPT implements DetallePerfilUsuarioCT.presenter {

    private DetallePerfilUsuarioCT.view view;
    private AyudaDonante ayudaDonante;
    private AyudasRepository ayudasRepository;
    private LoginRepository loginRepository;

    public DetallePerfilUsuarioPT(DetallePerfilUsuarioCT.view view, AyudaDonante ayudaDonante, Context context) {
        this.view = view;
        this.ayudaDonante = ayudaDonante;
        this.ayudasRepository = AyudasRepository.getInstance();
        this.loginRepository = LoginRepository.getInstance(new LoginDataSource(context));
    }

    @Override
    public void obtenerInteresados() {
        loginRepository.getUser(new CallbackResponse<LoggedInUser>() {
            @Override
            public void onResponse(LoggedInUser object) {
                view.setInfoUsuario(object);
                ayudasRepository.obtenerInteresadosDeAyuda(ayudaDonante.getId(), object.getId(), callbackResponse);
            }

            @Override
            public void onError(String msg) {
                view.showError(R.string.errorCargandoInfoUser);
            }
        });
    }

    @Override
    public void aceptarSolicitud(SolicitudAyuda sa) {
        ayudasRepository.cambiarEstadoSolicitud(sa.getId(), EEstados.APROBADA, callbackResponseCambio);
    }

    @Override
    public void rechazarSolicitud(SolicitudAyuda sa) {
        ayudasRepository.cambiarEstadoSolicitud(sa.getId(), EEstados.RECHAZADA, callbackResponseCambio);
    }

    private CallbackResponse<List<SolicitudAyuda>> callbackResponse = new CallbackResponse<List<SolicitudAyuda>>() {
        @Override
        public void onResponse(List<SolicitudAyuda> solicitudes) {
            view.setSolicitudes(solicitudes);
        }

        @Override
        public void onError(String msg) {
            view.showError(R.string.errorCargandoListaAyudas);
        }
    };

    private CallbackResponse<Object> callbackResponseCambio = new CallbackResponse<Object>() {
        @Override
        public void onResponse(Object solicitudes) {
            view.showError(R.string.estadoCambiado);
            obtenerInteresados();
        }

        @Override
        public void onError(String msg) {
            view.showError(R.string.errorCambiandoEstado);
        }
    };
}
