package co.com.yelphou.view.perfilUsuario.receptor;

import java.util.List;

import co.com.yelphou.model.AyudaDonante;
import co.com.yelphou.model.LoggedInUser;
import co.com.yelphou.model.SolicitudAyuda;

public interface PerfilUsuarioCT {

    interface view {

        void setUserInfo(LoggedInUser object);
        void showError(Integer errorString);

        void setAyudas(List<SolicitudAyuda> object);

        void logout();
    }

    interface presenter {

        void getInfoUsuario();

        void logout();
    }
}
