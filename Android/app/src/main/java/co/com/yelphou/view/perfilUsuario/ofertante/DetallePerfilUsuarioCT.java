package co.com.yelphou.view.perfilUsuario.ofertante;

import java.util.List;

import co.com.yelphou.model.LoggedInUser;
import co.com.yelphou.model.SolicitudAyuda;

public interface DetallePerfilUsuarioCT {
    interface view {
        void showError(Integer errorString);

        void setSolicitudes(List<SolicitudAyuda> solicitudes);

        void setInfoUsuario(LoggedInUser object);
    }

    interface presenter {
        void obtenerInteresados();

        void aceptarSolicitud(SolicitudAyuda sa);

        void rechazarSolicitud(SolicitudAyuda sa);
    }
}
