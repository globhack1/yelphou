package co.com.yelphou.view.crear_oferta;

import android.widget.Spinner;

public interface CrearOfertaCT {


    interface view{

        void quitarTodoBackground();

        void ponerBackground(int pos);

        void mostrarToast(String text);

        void limpiarCampos();

        void mostrarAgradecimiento();

        void hideKeyboard();

        void setNombreUsuario(String nombre);

    }

    interface presenter{

        void clickImagen(int pos);

        void clickGuardar(String descripcion, int localidad);

        void setAdapterLocalidad(Spinner localidad);



    }

}
