package co.com.yelphou.view.register.data;

import android.util.Log;

import com.google.gson.Gson;

import java.util.List;

import co.com.yelphou.RetrofitClientInstance;
import co.com.yelphou.data.OnGetDatabase;
import co.com.yelphou.data.remote.RegistroService;
import co.com.yelphou.model.Localidad;
import co.com.yelphou.model.TipoDocumento;
import co.com.yelphou.model.TipoUsuario;
import co.com.yelphou.model.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class RegistroRepository {


    Retrofit retrofit;
    RegistroService service;

    private static RegistroRepository instance;

    public static RegistroRepository getInstance(){
        if(instance == null){
            instance = new RegistroRepository();
        }
        return instance;
    }

    private RegistroRepository() {
        this.retrofit = RetrofitClientInstance.getRetrofitInstance();
        service = retrofit.create(RegistroService.class);
    }

    public void registro(User user, final OnGetDatabase<String> callback){

         service.registro(user).enqueue(new Callback<User>() {
             @Override
             public void onResponse(Call<User> call, Response<User> response) {
                 if(response.body() != null){
                     callback.onGet("true");
                 } else {
                     callback.onGet(null);
                 }
                 String data = new Gson().toJson(response.body());
                 Log.e("data", "registro:  "+data );
                 Log.e("Retro", "onResponse: " );
             }

             @Override
             public void onFailure(Call<User> call, Throwable t) {
                callback.onGet(null);
                 Log.e("Retro", "onFailure: "+t.fillInStackTrace() );
                t.fillInStackTrace();
             }
         });
    }

    public void obtenerTipoDocumento(OnGetDatabase<List<TipoDocumento>> callback){
        service.obtenerTipoDocumento().enqueue(new Callback<List<TipoDocumento>>() {
            @Override
            public void onResponse(Call<List<TipoDocumento>> call, Response<List<TipoDocumento>> response) {
                callback.onGet(response.body());
            }

            @Override
            public void onFailure(Call<List<TipoDocumento>> call, Throwable t) {
                callback.onGet(null);
            }
        });
    }

    public void obtenerLocalidades(OnGetDatabase<List<Localidad>> callback){
        service.obtenerLocalidades().enqueue(new Callback<List<Localidad>>() {
            @Override
            public void onResponse(Call<List<Localidad>> call, Response<List<Localidad>> response) {
                callback.onGet(response.body());
            }

            @Override
            public void onFailure(Call<List<Localidad>> call, Throwable t) {
                callback.onGet(null);
            }
        });
    }


    public void obtenerTipoUsuario(OnGetDatabase<List<TipoUsuario>> callback){
        service.obtenerTipoUsuario().enqueue(new Callback<List<TipoUsuario>>() {
            @Override
            public void onResponse(Call<List<TipoUsuario>> call, Response<List<TipoUsuario>> response) {
                callback.onGet(response.body());
            }

            @Override
            public void onFailure(Call<List<TipoUsuario>> call, Throwable t) {
                callback.onGet(null);
            }
        });
    }







}
