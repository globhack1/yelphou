package co.com.yelphou.view.verOfertas;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import co.com.yelphou.R;
import co.com.yelphou.view.perfilUsuario.ofertante.PerfilUsuarioActivity;
import co.com.yelphou.view.perfilUsuario.receptor.PerfilReceptorActivity;

public class VerOfertasVW extends AppCompatActivity implements VerOfertasCT.view {

    Spinner categoria;
    RecyclerView ofertas;
    TextView nombre_usuario;
    ImageView icono_usuario;

    VerOfertasCT.presenter presenter;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_ofertas_vw);

        categoria = findViewById(R.id.categoria);
        ofertas = findViewById(R.id.ofertas);
        nombre_usuario = findViewById(R.id.nombre_usuario);
        icono_usuario = findViewById(R.id.icono_usuario);

        presenter = new VerOfertasPT(this, this);

        presenter.setAdapterCategoria(categoria);

        presenter.setAdapterOfertas(ofertas);

        context = this;
        nombre_usuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abrirPerfil();
            }
        });

        icono_usuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abrirPerfil();
            }
        });

        categoria.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.e("Oferta", "onItemSelected: "+position );
                presenter.cambiaCategoria(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void abrirPerfil(){
        startActivity(new Intent(context, PerfilReceptorActivity.class));
    }

    @Override
    public void setNombreUsuario(String nombre) {
        nombre_usuario.setText(nombre);
    }

    @Override
    public void mostrarToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
    }
}
