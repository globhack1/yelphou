package co.com.yelphou.data;

public interface OnInsertDatabase<T> {
    void onInsert(T obj);
}
