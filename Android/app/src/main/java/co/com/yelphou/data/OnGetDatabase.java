package co.com.yelphou.data;

public interface OnGetDatabase<T> {

    void onGet(T obj);
}
