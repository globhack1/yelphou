package co.com.yelphou.view.perfilUsuario.receptor.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.lang.ref.WeakReference;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.com.yelphou.R;
import co.com.yelphou.model.SolicitudAyuda;

public class AyudaSolicitudAdapter extends RecyclerView.Adapter<AyudaSolicitudAdapter.ViewHolder>  {

    private AceptarRechazarCallback onAceptarRechazar;
    private List<SolicitudAyuda> solicitudes;

    public AyudaSolicitudAdapter(AceptarRechazarCallback onAceptarRechazar) {

        this.onAceptarRechazar = onAceptarRechazar;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View contactView = inflater.inflate(R.layout.item_ayuda_solicitudes, parent, false);

        return new ViewHolder(contactView, onAceptarRechazar);
    }

    @Override
    public void onBindViewHolder(@NonNull AyudaSolicitudAdapter.ViewHolder holder, int position) {
        SolicitudAyuda sa = this.solicitudes.get(position);
        holder.usuario.setText("Interesado");
        holder.descripcion.setText(sa.getDescripcion());
        holder.localidad.setText(sa.getEstado());
    }

    @Override
    public int getItemCount() {
        return solicitudes == null ? 0 : solicitudes.size();
    }

    public void setData(List<SolicitudAyuda> solicitudes) {
        this.solicitudes = solicitudes;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvUsuario)
        TextView usuario;
        @BindView(R.id.tvDescripcion)
        TextView descripcion;
        @BindView(R.id.tvLocalidad)
        TextView localidad;
        @BindView(R.id.btnAceptar)
        ImageButton aceptar;
        @BindView(R.id.btnRechazar)
        ImageButton rechazar;
        private WeakReference<AceptarRechazarCallback> listenerRef;

        @OnClick(R.id.btnAceptar)
        void onAceptar(){
            listenerRef.get().aceptar(solicitudes.get(getAdapterPosition()));
        }

        @OnClick(R.id.btnRechazar)
        void onRechazar(){
            listenerRef.get().rechazar(solicitudes.get(getAdapterPosition()));
        }

        ViewHolder(View itemView, AceptarRechazarCallback listener) {
            super(itemView);
            listenerRef = new WeakReference<>(listener);
            ButterKnife.bind(this, itemView);
        }
    }
}
