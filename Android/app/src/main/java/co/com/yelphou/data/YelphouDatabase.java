package co.com.yelphou.data;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import co.com.yelphou.data.db.User;
import co.com.yelphou.data.user.UserDAO;


@Database(entities = {User.class}, version = 1, exportSchema = false)
public abstract class YelphouDatabase extends RoomDatabase {

    public abstract UserDAO userDAO();

    private static volatile YelphouDatabase INSTANCE;

    public static YelphouDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (YelphouDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            YelphouDatabase.class, "YelphouDatabase")
                            .build();
                }
            }
        }
        return INSTANCE;
    }





}
