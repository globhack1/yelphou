package co.com.yelphou.view.login.data;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import co.com.yelphou.data.OnGetDatabase;
import co.com.yelphou.data.remote.CallbackResponse;
import co.com.yelphou.data.user.UserRepository;
import co.com.yelphou.model.LoggedInUser;
import co.com.yelphou.data.db.User;

/**
 * Class that requests authentication and user information from the remote data source and
 * maintains an in-memory cache of login status and user credentials information.
 */
public class LoginRepository {

    private static volatile LoginRepository instance;
    private LoginDataSource dataSource;

    private LoginRepository(LoginDataSource dataSource) {
        this.dataSource = dataSource;
    }

    public static LoginRepository getInstance(LoginDataSource dataSource) {
        if (instance == null) {
            instance = new LoginRepository(dataSource);
        }
        return instance;
    }

    public void getUser(CallbackResponse<LoggedInUser> callbackResponse) {
        dataSource.getUser(callbackResponse);
    }

    public void logout() {
        dataSource.logout();
    }

    public void login(String username, String password, CallbackResponse<LoggedInUser> callbackResponse) {
        dataSource.login(username, password, callbackResponse);
    }
}
