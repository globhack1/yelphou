package co.com.yelphou.view.crear_oferta.data;

import android.util.Log;

import com.google.gson.Gson;

import java.util.List;

import co.com.yelphou.RetrofitClientInstance;
import co.com.yelphou.data.OnGetDatabase;
import co.com.yelphou.data.remote.OfertaService;
import co.com.yelphou.data.remote.RegistroService;
import co.com.yelphou.model.Localidad;
import co.com.yelphou.model.Oferta;
import co.com.yelphou.model.User;
import co.com.yelphou.view.register.data.RegistroRepository;
import co.com.yelphou.view.verOfertas.ResponseCrearOferta;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class OfertaRepository {


    Retrofit retrofit;
    OfertaService service;

    private static OfertaRepository instance;

    public static OfertaRepository getInstance(){
        if(instance == null){
            instance = new OfertaRepository();
        }
        return instance;
    }

    private OfertaRepository() {
        this.retrofit = RetrofitClientInstance.getRetrofitInstance();
        service = retrofit.create(OfertaService.class);
    }

    public void crearOferta(Oferta oferta, final OnGetDatabase<String> callback){

        service.crearOferta(oferta).enqueue(new Callback<ResponseCrearOferta>() {
            @Override
            public void onResponse(Call<ResponseCrearOferta> call, Response<ResponseCrearOferta> response) {
                if(response.body()!=null){
                    callback.onGet("true");
                } else {
                    callback.onGet(null);
                }

                String data = new Gson().toJson(response.body());
                Log.e("data", "registro:  "+data );

                Log.e("Retro", "onResponse: " );
            }

            @Override
            public void onFailure(Call<ResponseCrearOferta> call, Throwable t) {
                callback.onGet(null);
                Log.e("Retro", "onFailure: " );
                Log.e("Retro", "onFailure: "+t.fillInStackTrace() );
                t.fillInStackTrace();
            }
        });
    }

    public void obtenerLocalidades(OnGetDatabase<List<Localidad>> callback){
        service.obtenerLocalidades().enqueue(new Callback<List<Localidad>>() {
            @Override
            public void onResponse(Call<List<Localidad>> call, Response<List<Localidad>> response) {
                callback.onGet(response.body());
            }

            @Override
            public void onFailure(Call<List<Localidad>> call, Throwable t) {
                callback.onGet(null);
            }
        });
    }


}
