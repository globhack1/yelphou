package co.com.yelphou.view.login;

import android.content.Context;
import android.util.Patterns;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import co.com.yelphou.R;
import co.com.yelphou.data.remote.CallbackResponse;
import co.com.yelphou.view.login.data.LoginDataSource;
import co.com.yelphou.view.login.data.LoginRepository;
import co.com.yelphou.view.login.data.Result;
import co.com.yelphou.model.LoggedInUser;

class LoginPT implements LoginCT.presenter {

    private MutableLiveData<LoginFormState> loginFormState = new MutableLiveData<>();
    private MutableLiveData<LoginResult> loginResult = new MutableLiveData<>();
    private LoginCT.view view;
    private Context context;
    private LoginRepository loginRepository;

    public LoginPT(LoginCT.view view, Context context) {
        this.view = view;
        loginRepository = LoginRepository.getInstance(new LoginDataSource(context));
    }

     public LiveData<LoginFormState> getLoginFormState() {
        return loginFormState;
    }

    public LiveData<LoginResult> getLoginResult() {
        return loginResult;
    }

    public void loginDataChanged(String username, String password) {
        if (!isUserNameValid(username)) {
            loginFormState.setValue(new LoginFormState(R.string.invalid_username, null));
        } else if (!isPasswordValid(password)) {
            loginFormState.setValue(new LoginFormState(null, R.string.invalid_password));
        } else {
            loginFormState.setValue(new LoginFormState(true));
        }
    }

    public void login(String username, String password) {
        loginRepository.login(username, password, new CallbackResponse<LoggedInUser>() {
            @Override
            public void onResponse(LoggedInUser object) {
                loginResult.setValue(new LoginResult(new LoggedInUserView(object.getNombres(), object.getTipoUsuario())));
            }

            @Override
            public void onError(String msg) {
                loginResult.setValue(new LoginResult(R.string.login_failed));
            }
        });
    }

    // A placeholder username validation check
    private boolean isUserNameValid(String username) {
        if (username == null) {
            return false;
        }
        if (username.contains("@")) {
            return Patterns.EMAIL_ADDRESS.matcher(username).matches();
        } else {
            return !username.trim().isEmpty();
        }
    }

    // A placeholder password validation check
    private boolean isPasswordValid(String password) {
        return password != null && password.trim().length() > 4;
    }
}
