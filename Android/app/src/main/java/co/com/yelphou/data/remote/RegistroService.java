package co.com.yelphou.data.remote;

import java.util.List;

import co.com.yelphou.model.Localidad;
import co.com.yelphou.model.LoggedInUser;
import co.com.yelphou.model.LoginCredentials;
import co.com.yelphou.model.TipoDocumento;
import co.com.yelphou.model.TipoUsuario;
import co.com.yelphou.model.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface RegistroService  {

    @POST("usuarios/registrar")
    Call<User> registro(@Body User user);

    @GET("utilidades/tipoDocumentos")
    Call<List<TipoDocumento>> obtenerTipoDocumento();

    @GET("utilidades/localidades")
    Call<List<Localidad>> obtenerLocalidades();

    @GET("usuarios/tipoUsuarios")
    Call<List<TipoUsuario>> obtenerTipoUsuario();

    @POST("usuarios/login")
    Call<LoggedInUser> login(@Body LoginCredentials credentials);


}
