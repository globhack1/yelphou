package co.com.yelphou.data;

public interface OnUpdateDatabase {
    void onUpdate(int rowsAffected);
}
