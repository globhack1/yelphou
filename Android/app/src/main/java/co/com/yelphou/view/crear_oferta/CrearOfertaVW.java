package co.com.yelphou.view.crear_oferta;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import co.com.yelphou.R;
import co.com.yelphou.view.perfilUsuario.ofertante.PerfilUsuarioActivity;
import co.com.yelphou.view.perfilUsuario.receptor.PerfilReceptorActivity;

public class CrearOfertaVW extends AppCompatActivity implements CrearOfertaCT.view {


    ImageView alimentos, ropa, hacer_tramite, acompanamiento, reciclaje, otros, imagen_agradec, icono_usuario;
    EditText descripcion;
    Spinner localidad;
    ImageButton guardar;
    TextView nombre_usuario;

    CrearOfertaCT.presenter presenter;

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_oferta_vw);

        nombre_usuario = findViewById(R.id.nombre_usuario);
        icono_usuario = findViewById(R.id.icono_usuario);
        alimentos = findViewById(R.id.alimentos);
        ropa = findViewById(R.id.ropa);
        hacer_tramite = findViewById(R.id.hacer_tramite);
        acompanamiento = findViewById(R.id.acompanamiento);
        reciclaje = findViewById(R.id.reciclaje);
        otros = findViewById(R.id.otros);
        imagen_agradec = findViewById(R.id.imagen_agradec);
        descripcion = findViewById(R.id.descripcion);
        localidad = findViewById(R.id.localidad);
        guardar = findViewById(R.id.guardar);

        imagen_agradec.setVisibility(View.GONE);

        imagen_agradec.setOnClickListener(v -> imagen_agradec.setVisibility(View.GONE));

        presenter = new CrearOfertaPT(this, this);

        alimentos.setOnClickListener(v -> presenter.clickImagen(1));

        ropa.setOnClickListener(v -> presenter.clickImagen(2));

        hacer_tramite.setOnClickListener(v -> presenter.clickImagen(3));

        acompanamiento.setOnClickListener(v -> presenter.clickImagen(4));

        reciclaje.setOnClickListener(v -> presenter.clickImagen(5));

        otros.setOnClickListener(v -> presenter.clickImagen(6));

        guardar.setOnClickListener(v -> {
            presenter.clickGuardar(descripcion.getText().toString(), localidad.getSelectedItemPosition());
        });
        context = this;
        nombre_usuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abrirPerfil();
            }
        });

        icono_usuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               abrirPerfil();
            }
        });

        presenter.setAdapterLocalidad(localidad);

    }

    private void abrirPerfil(){
        startActivity(new Intent(context, PerfilUsuarioActivity.class));
    }

    @Override
    public void quitarTodoBackground() {
        alimentos.setBackground(getDrawable(R.drawable.icono_fondo_blanco));
        ropa.setBackground(getDrawable(R.drawable.icono_fondo_blanco));
        hacer_tramite.setBackground(getDrawable(R.drawable.icono_fondo_blanco));
        acompanamiento.setBackground(getDrawable(R.drawable.icono_fondo_blanco));
        reciclaje.setBackground(getDrawable(R.drawable.icono_fondo_blanco));
        otros.setBackground(getDrawable(R.drawable.icono_fondo_blanco));
    }

    @Override
    public void ponerBackground(int pos) {
        switch (pos){
            case 1:
                alimentos.setBackground(getDrawable(R.drawable.icono_fondo_amarillo));
                break;
            case 2:
                ropa.setBackground(getDrawable(R.drawable.icono_fondo_amarillo));
                break;
            case 3:
                hacer_tramite.setBackground(getDrawable(R.drawable.icono_fondo_amarillo));
                break;
            case 4:
                acompanamiento.setBackground(getDrawable(R.drawable.icono_fondo_amarillo));
                break;
            case 5:
                reciclaje.setBackground(getDrawable(R.drawable.icono_fondo_amarillo));
                break;
            case 6:
                otros.setBackground(getDrawable(R.drawable.icono_fondo_amarillo));
                break;
        }
    }

    @Override
    public void mostrarToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
    }

    @Override
    public void limpiarCampos() {
        descripcion.setText("");
        localidad.setSelection(0);
        quitarTodoBackground();
    }

    @Override
    public void mostrarAgradecimiento() {
        imagen_agradec.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(this);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void setNombreUsuario(String nombre) {
        nombre_usuario.setText(nombre);
    }
}
