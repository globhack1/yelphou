package co.com.yelphou.view.verOfertas;

public class ResponseCrearOferta {

    public int id;
    public String estado;
    public String descripcion;
    public String fechaRegistro;
    public String gps;
    public int idcategoriaAyudaId;
    public String categoriaAyuda;
    public int idusuarioOferenteId;
    public String usuarioOferente;
    public int idlocalidadId;
    public String localidad;

}
