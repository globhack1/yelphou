package co.com.yelphou.view.login;

import co.com.yelphou.model.TipoUsuario;

/**
 * Class exposing authenticated user details to the UI.
 */
public class LoggedInUserView {
    private String displayName;
    //... other data fields that may be accessible to the UI

    LoggedInUserView(String displayName, TipoUsuario tipoUsuario) {
        this.displayName = displayName;
        this.tipoUsuario = tipoUsuario;
    }

    String getDisplayName() {
        return displayName;
    }

    TipoUsuario tipoUsuario;
}
