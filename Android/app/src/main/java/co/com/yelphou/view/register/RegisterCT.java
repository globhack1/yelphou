package co.com.yelphou.view.register;

import android.widget.Spinner;

public interface RegisterCT {

    interface view {

        void mostrarAlerta(String text);
        void mostrarToast(String text);

        void mostrarLoading();
        void ocultarLoading();

    }

    interface presenter {

        void setAdaterTipo_Doc(Spinner tipo_doc);
        void setAdapterLocalidad(Spinner localidad);
        void setAdapterTipoUsuario(Spinner tipo_usua);

        void registro(String correo, String contra, String nombre, String apellido, int tipo_doc, String numero_doc, String fecha_nac, String direccion, int localidad, int tipo_user);

    }


}
