package co.com.yelphou.data.remote;

import java.util.List;

import co.com.yelphou.model.Localidad;
import co.com.yelphou.model.Oferta;
import co.com.yelphou.view.verOfertas.ResponseCrearOferta;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface OfertaService {


    @POST("ayudas/registrar")
    Call<ResponseCrearOferta> crearOferta(@Body Oferta oferta);

    @GET("utilidades/localidades")
    Call<List<Localidad>> obtenerLocalidades();

}
