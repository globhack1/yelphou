package co.com.yelphou.model;

import lombok.Data;

@Data
public class SolicitudAyuda {
    private Integer id;
    private String estado;
    private String descripcion;
    private AyudaDonante ayuda;
}
