package co.com.yelphou.model;

public enum EEstados {
    PENDIENTE, APROBADA, RECHAZADA;
}
