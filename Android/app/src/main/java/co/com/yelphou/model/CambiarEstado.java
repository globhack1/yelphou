package co.com.yelphou.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CambiarEstado {
    private int id;
    private String estado;

}
