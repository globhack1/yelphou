package co.com.yelphou.view.login.data;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import co.com.yelphou.RetrofitClientInstance;
import co.com.yelphou.data.OnGetDatabase;
import co.com.yelphou.data.db.User;
import co.com.yelphou.data.remote.CallbackResponse;
import co.com.yelphou.data.remote.RegistroService;
import co.com.yelphou.data.user.UserRepository;
import co.com.yelphou.model.LoggedInUser;
import co.com.yelphou.model.LoginCredentials;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
public class LoginDataSource {

    private final UserRepository userRepository;
    private RegistroService registroService;
    private LoggedInUser user;

    public LoginDataSource(Context context) {
        userRepository = new UserRepository(context);
        Retrofit retrofit = RetrofitClientInstance.getRetrofitInstance();
        registroService = retrofit.create(RegistroService.class);
    }

    public void getUser(CallbackResponse<LoggedInUser> callback) {
        if (user == null) {
            userRepository.getUser(1L, new OnGetDatabase<User>() {
                @Override
                public void onGet(User obj) {
                    if (obj == null) {
                        callback.onResponse(null);
                    } else {
                        user = new Gson().fromJson(obj.data, LoggedInUser.class);
                        callback.onResponse(user);
                    }
                }
            });
        } else {
            callback.onResponse(user);
        }

    }

    public void login(String username, String password, CallbackResponse<LoggedInUser> callback) {

        registroService.login(new LoginCredentials(username, password)).enqueue(new Callback<LoggedInUser>() {
            @Override
            public void onResponse(Call<LoggedInUser> call, Response<LoggedInUser> response) {
                if(!response.isSuccessful()){
                    callback.onError("Usuario o contraseña no valido");
                    return;
                }

                LoggedInUser loggedInUser = response.body();
                if(loggedInUser != null) {
                    User userDb = new User();
                    userDb.id = 1;

                    String da = new Gson().toJson(loggedInUser);
                    Log.e("Login", "onResponse: " + da);

                    try {
                        JSONObject o = new JSONObject(da);
                        userDb.data = o.toString();
                        Log.e("Login", "onResponse: 111 " + o);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    userRepository.insertUser(userDb);
                }
                callback.onResponse(response.body());
            }

            @Override
            public void onFailure(Call<LoggedInUser> call, Throwable t) {
                callback.onError(t.getMessage());
            }
        });
    }

    public void logout() {
        userRepository.getUser(1L, new OnGetDatabase<User>() {
            @Override
            public void onGet(User obj) {
                userRepository.deleteUser(obj);
            }
        });
        user = null;
    }
}
