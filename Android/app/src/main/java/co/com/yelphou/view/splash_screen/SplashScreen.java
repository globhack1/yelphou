package co.com.yelphou.view.splash_screen;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;

import co.com.yelphou.data.OnGetDatabase;
import co.com.yelphou.data.db.User;
import co.com.yelphou.data.user.UserRepository;
import co.com.yelphou.model.LoggedInUser;
import co.com.yelphou.view.crear_oferta.CrearOfertaVW;
import co.com.yelphou.view.login.LoginActivity;
import co.com.yelphou.view.verOfertas.VerOfertasVW;


public class SplashScreen extends AppCompatActivity {

    private UserRepository userRepository;

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_splash_screen);
        context = this;
        userRepository = new UserRepository(this);

        userRepository.getUser(1L, new OnGetDatabase<User>() {
            @Override
            public void onGet(User obj) {
                if (obj == null || obj.data == null) {
                    if (obj != null) {
                        userRepository.deleteUser(obj);
                    }
                    Intent in = new Intent(context, LoginActivity.class);
                    startActivity(in);
                    finish();
                    return;
                }
                String da = new Gson().toJson(obj.data);
                da = da.substring(0, da.length() - 1);
                da = da.substring(1);
                da = da.replace("\\\"", "\"");


                LoggedInUser user = new Gson().fromJson(da, LoggedInUser.class);
                if (user.getTipoUsuario().id == 1) {
                    Intent in = new Intent(context, CrearOfertaVW.class);
                    startActivity(in);
                    finish();
                } else {
                    Intent in = new Intent(context, VerOfertasVW.class);
                    startActivity(in);
                    finish();
                }
            }
        });

        try {
            Thread.sleep(1000);
        } catch (
                InterruptedException e) {
            e.printStackTrace();
        }
    }

}

