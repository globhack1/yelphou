package co.com.yelphou.view.verOfertas;

import android.widget.RelativeLayout;
import android.widget.Spinner;

import androidx.recyclerview.widget.RecyclerView;

public interface VerOfertasCT {

    interface view {


        void setNombreUsuario(String nombre);

        void mostrarToast(String text);

    }

    interface presenter {

        void cambiaCategoria(int pos);
        void setAdapterCategoria(Spinner categoria);
        void setAdapterOfertas(RecyclerView ofertas);

    }
}
