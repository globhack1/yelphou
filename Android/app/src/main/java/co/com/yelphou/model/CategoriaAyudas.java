package co.com.yelphou.model;

import lombok.Data;

@Data
public class CategoriaAyudas {
    private Integer id;
    private String descripcion;
}
