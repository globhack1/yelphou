package co.com.yelphou.model;

import lombok.Data;

@Data
public class AyudaDonante {
    private Integer id;
    private String estado;
    private String descripcion;
    private String gps;
    private CategoriaAyudas categoriaAyuda;
    private Localidad localidad;
    private LoggedInUser usuarioOferente;
}
