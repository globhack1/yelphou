package co.com.yelphou.view.verOfertas.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import java.util.List;

import co.com.yelphou.R;
import co.com.yelphou.model.AyudaActiva;
import co.com.yelphou.view.verOfertas.util.SolicitarOferta;

public class OfertasAdapter extends RecyclerView.Adapter<OfertasAdapter.OfertasViewHolder> {


    List<AyudaActiva> ayudas;
    Context context;
    SolicitarOferta callback;

    public OfertasAdapter(Context context, List<AyudaActiva> ayudas,SolicitarOferta callback ) {
        this.ayudas = ayudas;
        this.context = context;
        this.callback = callback;
    }

    public void setList(List<AyudaActiva> ayudas){
        this.ayudas = ayudas;
    }

    @NonNull
    @Override
    public OfertasViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_oferta, parent, false);

        return new OfertasViewHolder(view, callback);
    }

    @Override
    public void onBindViewHolder(@NonNull OfertasViewHolder holder, int position) {

        AyudaActiva ayudaActiva = ayudas.get(position);

        holder.descripcion.setVisibility(View.GONE);
        holder.aplicar.setVisibility(View.GONE);

        holder.descripcion_oferta.setText(ayudaActiva.descripcion);

        holder.localidad.setText(ayudaActiva.localidad.nombre);

        String data = new Gson().toJson(ayudaActiva);
        Log.e("data", "registro:  "+data );

        if(ayudaActiva.estrella){
            holder.estrella.setImageDrawable(context.getDrawable(R.drawable.icono_aplico_oferta));
        } else {
            holder.estrella.setImageDrawable(context.getDrawable(R.drawable.icono_aplico_oferta_desactivado));
        }

        switch (ayudaActiva.categoriaAyuda.getId()){
            case 1:
                holder.imagen_categoria.setImageDrawable(context.getDrawable(R.drawable.icono_alimentos_receptor));
                break;
            case 2:
                holder.imagen_categoria.setImageDrawable(context.getDrawable(R.drawable.icono_ropa_receptor));
                break;
            case 3:
                holder.imagen_categoria.setImageDrawable(context.getDrawable(R.drawable.icono_tramites_receptor));
                break;
            case 4:
                holder.imagen_categoria.setImageDrawable(context.getDrawable(R.drawable.icono_acompanamientos_receptor));
                break;
            case 5:
                holder.imagen_categoria.setImageDrawable(context.getDrawable(R.drawable.icono_reciclaje_receptor));
                break;
            case 6:
                holder.imagen_categoria.setImageDrawable(context.getDrawable(R.drawable.icono_otros_receptor));
                break;
        }


        holder.estrella.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.descripcion.setVisibility(View.VISIBLE);
                holder.aplicar.setVisibility(View.VISIBLE);
            }
        });

        holder.aplicarText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.descripcion.setVisibility(View.VISIBLE);
                holder.aplicar.setVisibility(View.VISIBLE);
            }
        });


        holder.aplicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.descripcion.setVisibility(View.GONE);
                holder.aplicar.setVisibility(View.GONE);
                holder.callback.solicitar(position, holder.descripcion.getText().toString());
                holder.descripcion.setText("");
            }
        });

    }


    @Override
    public int getItemCount() {
        if(ayudas == null){
            return 0;
        } else {
            return ayudas.size();
        }

    }


    public class OfertasViewHolder extends RecyclerView.ViewHolder {

        ImageView imagen_categoria, estrella;
        TextView localidad, descripcion_oferta, aplicarText;
        EditText descripcion;
        ImageButton aplicar;
        SolicitarOferta callback;

        public OfertasViewHolder(@NonNull View itemView, SolicitarOferta callback) {
            super(itemView);

            imagen_categoria = itemView.findViewById(R.id.imagen_categoria);
            estrella = itemView.findViewById(R.id.estrella);
            aplicarText = itemView.findViewById(R.id.aplicarText);
            localidad = itemView.findViewById(R.id.localidad);
            descripcion = itemView.findViewById(R.id.descripcion);
            descripcion_oferta = itemView.findViewById(R.id.descripcion_oferta);
            aplicar = itemView.findViewById(R.id.aplicar);

            this.callback = callback;
        }
    }
}
