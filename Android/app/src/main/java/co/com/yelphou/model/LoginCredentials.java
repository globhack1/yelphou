package co.com.yelphou.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LoginCredentials {
    private String correo;
    private String contrasena;

}
