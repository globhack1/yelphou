package co.com.yelphou.view.register;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

import co.com.yelphou.R;

public class RegisterVW extends AppCompatActivity implements RegisterCT.view {


    private RegisterCT.presenter presenter;

    private TextView fecha_nacimiento;

    private EditText correo, contrasena, nombre, apellido, identificacion, direccion;

    private Spinner tipo_identificacion, localidad, tipo_usuario;

    private ImageButton boton_atras, registrar;

    private RelativeLayout loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_vw);

        correo = findViewById(R.id.correo);
        contrasena = findViewById(R.id.contrasena);
        nombre = findViewById(R.id.nombre);
        apellido = findViewById(R.id.apellido);
        identificacion = findViewById(R.id.identificacion);
        fecha_nacimiento = findViewById(R.id.fecha_nacimiento);
        direccion = findViewById(R.id.direccion);
        tipo_identificacion = findViewById(R.id.tipo_identificacion);
        localidad = findViewById(R.id.localidad);
        tipo_usuario = findViewById(R.id.tipo_usuario);
        boton_atras = findViewById(R.id.boton_atras);
        registrar = findViewById(R.id.registrar);

        loading = findViewById(R.id.loading);

        loading.setVisibility(View.VISIBLE);

        boton_atras.setOnClickListener(v -> finish());

        presenter = new RegisterPT(this, this);

        registrar.setOnClickListener(v -> presenter.registro(
                correo.getText().toString(),
                contrasena.getText().toString(),
                nombre.getText().toString(),
                apellido.getText().toString(),
                tipo_identificacion.getSelectedItemPosition(),
                identificacion.getText().toString(),
                fecha_nacimiento.getText().toString(),
                direccion.getText().toString(),
                localidad.getSelectedItemPosition(),
                tipo_usuario.getSelectedItemPosition()
        ));

        presenter.setAdapterLocalidad(localidad);
        presenter.setAdaterTipo_Doc(tipo_identificacion);
        presenter.setAdapterTipoUsuario(tipo_usuario);


        final DatePickerDialog.OnDateSetListener listener = (datePicker, year, month, day) -> {
            fecha_nacimiento.setText(year+"/"+((month < 10)?("0"+month):month)+"/"+ ((day < 10)?("0"+day):day));
            fecha_nacimiento.setTextColor(Color.BLACK);
        };

        fecha_nacimiento.setOnClickListener(v -> clickSelectDate(listener));


    }

    private void clickSelectDate(DatePickerDialog.OnDateSetListener mDateSetListener){
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);

        DatePickerDialog dialog = new DatePickerDialog(this, mDateSetListener, year, month, day);
        dialog.show();
    }

    @Override
    public void mostrarAlerta(String text) {
        new AlertDialog.Builder(this)
                .setTitle("")
                .setMessage(text)
                .setPositiveButton("OK", (dialog, which) -> finish())
                .show();
    }

    @Override
    public void mostrarToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
    }

    @Override
    public void mostrarLoading() {
        loading.setVisibility(View.VISIBLE);
    }

    @Override
    public void ocultarLoading() {
        loading.setVisibility(View.GONE);
    }


}
