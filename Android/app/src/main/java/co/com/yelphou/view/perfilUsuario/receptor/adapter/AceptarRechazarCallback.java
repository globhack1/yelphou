package co.com.yelphou.view.perfilUsuario.receptor.adapter;

import co.com.yelphou.model.SolicitudAyuda;

public interface AceptarRechazarCallback {
    void aceptar(SolicitudAyuda sa);
    void rechazar(SolicitudAyuda sa);
}
