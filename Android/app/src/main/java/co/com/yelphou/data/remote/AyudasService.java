package co.com.yelphou.data.remote;

import java.util.List;

import co.com.yelphou.model.AyudaActiva;
import co.com.yelphou.model.AyudaDonante;
import co.com.yelphou.model.CambiarEstado;
import co.com.yelphou.model.SolicitudAyuda;
import co.com.yelphou.view.verOfertas.AplicarAyuda;
import co.com.yelphou.view.verOfertas.ResponseAplicar;
import lombok.Getter;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface AyudasService {

    @GET("ayudas/ayudasUsuario")
    Call<List<AyudaDonante>> getAyudasXUsuario(@Query("usuarioId") int id);

    @GET("ayudas/solicitudesUsuario")
    Call<List<SolicitudAyuda>> getSolicitudesXUsuario(@Query("usuarioId") int id);

    @GET("ayudas/ayudasActivas")
    Call<List<AyudaActiva>> getAyudasReceptor(@Query("localidadId") int localidad, @Query("categoriaId") int categoria);
    
    @GET("ayudas/solicitudesPorAyuda")
    Call<List<SolicitudAyuda>> getSolicitudesPorAyuda(@Query("ayudaId") int id);

    @PUT("ayudas/cambiarEstadoSolicitud")
    Call<Object> cambiarEstadoSolicitud(@Body CambiarEstado cambiarEstado);

    @POST("ayudas/aplicar")
    Call<ResponseAplicar> aplicarAyuda(@Body AplicarAyuda aplicarAyuda);

}
