package co.com.yelphou.view.register.util;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import co.com.yelphou.model.TipoUsuario;

public class SpinnerTipoUsuarioAdapter extends ArrayAdapter<TipoUsuario> {


    private Context context;
    private TipoUsuario[] values;

    public SpinnerTipoUsuarioAdapter(Context context, int textViewResourceId, TipoUsuario[] values) {
        super(context, textViewResourceId, values);
        this.values = values;
    }

    @Override
    public int getCount(){
        return values.length;
    }

    @Override
    public TipoUsuario getItem(int position){
        return values[position];
    }

    @Override
    public long getItemId(int position){
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setTextColor(Color.BLACK);

        label.setText(values[position].descripcion);

        return label;
    }


    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {

        TextView label = (TextView) super.getDropDownView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setText(values[position].descripcion);

        return label;
    }

}
