package co.com.yelphou.data.repository;

import java.util.List;

import co.com.yelphou.RetrofitClientInstance;
import co.com.yelphou.data.remote.AyudasService;
import co.com.yelphou.data.remote.CallbackResponse;
import co.com.yelphou.model.AyudaDonante;
import co.com.yelphou.model.CambiarEstado;
import co.com.yelphou.model.EEstados;
import co.com.yelphou.model.SolicitudAyuda;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class AyudasRepository {
    private static AyudasRepository instance;
    private AyudasService ayudasService;

    public static AyudasRepository getInstance() {
        if (instance == null) {
            instance = new AyudasRepository();
        }
        return instance;
    }

    private AyudasRepository() {
        Retrofit retrofit = RetrofitClientInstance.getRetrofitInstance();
        ayudasService = retrofit.create(AyudasService.class);
    }

    public void cambiarEstadoSolicitud(Integer id, EEstados aprobada, CallbackResponse<Object> callback) {
        ayudasService.cambiarEstadoSolicitud(new CambiarEstado(id, aprobada.name())).enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                callback.onResponse(response.body());
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                callback.onError(t.getMessage());
            }
        });
    }

    public void getAyudasXUsuario(int id, CallbackResponse<List<AyudaDonante>> resp) {
        ayudasService.getAyudasXUsuario(id).enqueue(new Callback<List<AyudaDonante>>() {
            @Override
            public void onResponse(Call<List<AyudaDonante>> call, Response<List<AyudaDonante>> response) {
                resp.onResponse(response.body());
            }

            @Override
            public void onFailure(Call<List<AyudaDonante>> call, Throwable t) {
                resp.onError(t.getMessage());
            }
        });
    }

    public void obtenerInteresadosDeAyuda(int idAyuda, int idUsuario, CallbackResponse<List<SolicitudAyuda>> callback) {

        ayudasService.getSolicitudesPorAyuda(idAyuda).enqueue(new Callback<List<SolicitudAyuda>>() {
            @Override
            public void onResponse(Call<List<SolicitudAyuda>> call, Response<List<SolicitudAyuda>> response) {
                callback.onResponse(response.body());
            }

            @Override
            public void onFailure(Call<List<SolicitudAyuda>> call, Throwable t) {
                callback.onError(t.getMessage());
            }
        });
        /*
        List<SolicitudAyuda> sas = new ArrayList<>();
        SolicitudAyuda sa = new SolicitudAyuda();
        sa.setId(1);
        sa.setDescripcion("algo");
        sa.setEstado("pendiente");

        SolicitudAyuda sa1 = new SolicitudAyuda();
        sa1.setId(2);
        sa1.setDescripcion("algo 2");
        sa1.setEstado("aceptada");

        sas.add(sa);
        sas.add(sa1);
        callback.onResponse(sas);

         */
    }

    public void getSolicitudesXUsuario(int id, CallbackResponse<List<SolicitudAyuda>> callback) {
        ayudasService.getSolicitudesXUsuario(id).enqueue(new Callback<List<SolicitudAyuda>>() {
            @Override
            public void onResponse(Call<List<SolicitudAyuda>> call, Response<List<SolicitudAyuda>> response) {
                callback.onResponse(response.body());
            }

            @Override
            public void onFailure(Call<List<SolicitudAyuda>> call, Throwable t) {
                callback.onError(t.getMessage());
            }
        });
    }
}
