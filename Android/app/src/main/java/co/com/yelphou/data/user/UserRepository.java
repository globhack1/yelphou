package co.com.yelphou.data.user;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import co.com.yelphou.data.OnGetDatabase;
import co.com.yelphou.data.OnUpdateDatabase;
import co.com.yelphou.data.YelphouDatabase;
import co.com.yelphou.data.db.User;

public class UserRepository {


    private YelphouDatabase database;

    public UserRepository(Context context){
        database = YelphouDatabase.getDatabase(context);
    }


    public void getUser(final Long id, final OnGetDatabase<User> callback){
        new AsyncTask<Void, Void, User>() {
            @Override
            protected User doInBackground(Void... voids) {
                return database.userDAO().getUser(id);
            }

            @Override
            protected void onPostExecute(User obj) {
                super.onPostExecute(obj);
                callback.onGet(obj);
            }
        }.execute();
    }

    public void deleteUser(final User user) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                database.userDAO().deleteUser(user);
                return null;
            }

        }.execute();
    }

    public void insertUser(final User user) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                Long id = database.userDAO().insertUser(user);
                return null;
            }

        }.execute();
    }

    public void updateUser(final User user, final OnUpdateDatabase callback) {
        new AsyncTask<Void, Void, Integer>() {
            @Override
            protected Integer doInBackground(Void... voids) {
                return database.userDAO().updateUser(user);
            }

            @Override
            protected void onPostExecute(Integer integer) {
                super.onPostExecute(integer);
                callback.onUpdate(integer);
            }
        }.execute();
    }




}
