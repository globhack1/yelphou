package co.com.yelphou.model;

public class User {

    public int tipoUsuarioId;
    public int tipoDocumentoId;
    public long documento;
    public String nombres;
    public String apellidos;
    public String correo;
    public int localidadId;
    public String direccion;
    public String contrasena;
    public String fechaNacimiento;

}
