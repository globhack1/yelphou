package co.com.yelphou.model;

import lombok.Data;

@Data
public class LoggedInUser {

    private int id;

    private TipoUsuario tipoUsuario;

    private TipoDocumento tipoDocumento;

    private String documento;

    private String nombres;

    private String apellidos;

    private String fechaNacimiento;

    private Localidad localidad;

    private String direccion;

}