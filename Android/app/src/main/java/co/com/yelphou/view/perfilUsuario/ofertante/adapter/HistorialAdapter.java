package co.com.yelphou.view.perfilUsuario.ofertante.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.com.yelphou.R;
import co.com.yelphou.model.AyudaDonante;
import lombok.Getter;

public class HistorialAdapter extends RecyclerView.Adapter<HistorialAdapter.ViewHolder>  {

    @Getter
    private List<AyudaDonante> ayudas;
    private final View.OnClickListener mOnClickListener;

    public HistorialAdapter(View.OnClickListener mOnClickListener) {

        this.mOnClickListener = mOnClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View contactView = inflater.inflate(R.layout.item_historia_ayuda, parent, false);
        contactView.setOnClickListener(mOnClickListener);
        return new ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(@NonNull HistorialAdapter.ViewHolder holder, int position) {
        AyudaDonante item  = ayudas.get(position);
        holder.categoria.setText(item.getCategoriaAyuda().getDescripcion());
        holder.estado.setText(item.getEstado());
        holder.cantSolicitudes.setText("0");
        int imagen = R.drawable.icono_acompanamientos_perfil;
        switch (item.getCategoriaAyuda().getId()){
            case 4: imagen = R.drawable.icono_acompanamientos_perfil; break;
            case 1: imagen = R.drawable.icono_alimentos_perfil; break;
            case 6: imagen = R.drawable.icono_otros_perfil; break;
            case 5: imagen = R.drawable.icono_reciclaje_perfil; break;
            case 2: imagen = R.drawable.icono_ropa_perfil; break;
            case 3: imagen = R.drawable.icono_tramites_perfil; break;
        }
        holder.imgCategoria.setImageResource(imagen);
    }

    @Override
    public int getItemCount() {
        return ayudas == null ? 0 : ayudas.size();
    }

    public void setData(List<AyudaDonante> object) {
        this.ayudas = object;
        this.notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvUsuario)
        TextView categoria;
        @BindView(R.id.tvDescripcion)
        TextView estado;
        @BindView(R.id.tvLocalidad)
        TextView cantSolicitudes;
        @BindView(R.id.ivCategoria)
        ImageView imgCategoria;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
