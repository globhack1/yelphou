package co.com.yelphou.view.perfilUsuario.receptor;

import android.content.Context;

import java.util.List;

import co.com.yelphou.R;
import co.com.yelphou.data.remote.CallbackResponse;
import co.com.yelphou.data.repository.AyudasRepository;
import co.com.yelphou.model.AyudaDonante;
import co.com.yelphou.model.LoggedInUser;
import co.com.yelphou.model.SolicitudAyuda;
import co.com.yelphou.view.login.data.LoginDataSource;
import co.com.yelphou.view.login.data.LoginRepository;

public class PerfilUsuarioPT implements PerfilUsuarioCT.presenter {
    private PerfilUsuarioCT.view view;
    private LoginRepository loginRepository;
    private AyudasRepository ayudasRepository;

    public PerfilUsuarioPT(PerfilUsuarioCT.view view, Context context) {
        this.view = view;
        this.loginRepository =  LoginRepository.getInstance(new LoginDataSource(context));
        this.ayudasRepository = AyudasRepository.getInstance();
    }

    @Override
    public void logout() {
        loginRepository.logout();
        view.logout();
    }

    @Override
    public void getInfoUsuario() {
        loginRepository.getUser(respuestaUsuario);
    }

    CallbackResponse<LoggedInUser> respuestaUsuario = new CallbackResponse<LoggedInUser>() {
        @Override
        public void onResponse(LoggedInUser object) {
            if(object == null)
                view.showError(R.string.errorCargandoInfoUser);
            else {
                view.setUserInfo(object);
                ayudasRepository.getSolicitudesXUsuario(object.getId(), respuestaAyudas);
            }
        }

        @Override
        public void onError(String msg) {
            view.showError(R.string.errorCargandoInfoUser);
        }
    };

    CallbackResponse<List<SolicitudAyuda>> respuestaAyudas = new CallbackResponse<List<SolicitudAyuda>>() {
        @Override
        public void onResponse(List<SolicitudAyuda> object) {
            view.setAyudas(object);
        }

        @Override
        public void onError(String msg) {
            view.showError(R.string.errorCargandoAyudas);
        }
    };
}
