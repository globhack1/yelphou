/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.yelphou.repositorios;

import co.com.yelphou.entidades.CategoriaAyuda;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Nicolas
 */
public interface IRepoCategoriaAyudas extends CrudRepository<CategoriaAyuda, Integer> {

}
