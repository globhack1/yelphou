/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.yelphou.repositorios;

import co.com.yelphou.entidades.SolicitudAyuda;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author PERSONAL
 */
@Repository
public interface IRepoSolicitudAyuda extends CrudRepository<SolicitudAyuda, Integer> {

    List<SolicitudAyuda> findByUsuarioReceptor(@Param(value = "usuarioReceptor") Integer usuarioReceptor);
    
    List<SolicitudAyuda> findByAyudaId(@Param(value = "ayudaId") Integer ayudaId);

}
