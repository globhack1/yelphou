/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.yelphou.repositorios;

import co.com.yelphou.entidades.Ayuda;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author PERSONAL
 */
public interface IRepoAyudas extends CrudRepository<Ayuda, Integer> {

    List<Ayuda> findByLocalidadId(@Param(value = "localidadId") Integer localidadId);

    List<Ayuda> findByLocalidadAndCategoria(@Param(value = "localidadId") Integer localidadId,
            @Param(value = "categoriaId") Integer categoriaId);

    List<Ayuda> findByUsuarioId(@Param(value = "usuarioId") Integer usuarioId);

}
