/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.yelphou.controladores.dto;

import lombok.Data;

/**
 *
 * @author Nicolas
 */
@Data
public class TipoUsuariosDto {

    private Integer id;
    private String descripcion;

}
