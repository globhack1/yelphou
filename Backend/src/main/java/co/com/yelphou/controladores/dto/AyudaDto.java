/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.yelphou.controladores.dto;

import co.com.yelphou.entidades.Localidad;
import lombok.Data;

/**
 *
 * @author PERSONAL
 */
@Data
public class AyudaDto {

    private Integer id;
    private String estado;
    private String descripcion;
    private String gps;
    private CategoriaAyudasDto categoriaAyuda;
    private LocalidadesDto localidad;
    private UsuarioDto usuarioOferente;

}
