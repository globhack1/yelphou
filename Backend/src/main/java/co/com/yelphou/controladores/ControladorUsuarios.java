/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.yelphou.controladores;

import co.com.yelphou.controladores.dto.LoginDto;
import co.com.yelphou.controladores.dto.RegistrarUsuarioDto;
import co.com.yelphou.servicios.ServicioUsuarios;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author PERSONAL
 */
@RestController()
@RequestMapping("/usuarios")
public class ControladorUsuarios {

    @Autowired
    private ServicioUsuarios servicioUsuarios;

    @PostMapping(value = "/registrar")
    public ResponseEntity<String> registrar(@RequestBody RegistrarUsuarioDto usuario) {
        return servicioUsuarios.registrar(usuario);
    }

    @GetMapping(value = "/tipoUsuarios")
    public ResponseEntity tipoUsuarios() {
        return new ResponseEntity<>(servicioUsuarios.tipoUsuarios(), HttpStatus.OK);
    }

    @PostMapping(value = "/login")
    public ResponseEntity<Object> login(@RequestBody LoginDto login) {
        return servicioUsuarios.login(login);
    }

}
