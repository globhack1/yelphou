/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.yelphou.controladores.dto;

import lombok.Data;

/**
 *
 * @author PERSONAL
 */
@Data
public class SolicitudAyudaDto {

    private Integer id;
    private String estado;
    private String descripcion;
    private AyudaDto ayuda;

}
