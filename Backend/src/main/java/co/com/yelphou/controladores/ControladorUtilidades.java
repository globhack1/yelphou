/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.yelphou.controladores;

import co.com.yelphou.servicios.ServicioUtilidades;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author PERSONAL
 */
@RestController()
@RequestMapping("/utilidades")
public class ControladorUtilidades {

    @Autowired
    private ServicioUtilidades servicioUtilidades;

    @GetMapping(value = "/tipoDocumentos")
    public ResponseEntity tipoDocumentos() {
        return new ResponseEntity<>(servicioUtilidades.tipoDocumentos(), HttpStatus.OK);
    }

    @GetMapping(value = "/localidades")
    public ResponseEntity localidades() {
        return new ResponseEntity<>(servicioUtilidades.localidades(), HttpStatus.OK);
    }

}
