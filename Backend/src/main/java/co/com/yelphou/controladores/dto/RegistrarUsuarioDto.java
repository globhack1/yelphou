/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.yelphou.controladores.dto;

import lombok.Data;

/**
 *
 * @author PERSONAL
 */
@Data
public class RegistrarUsuarioDto {

    private Integer tipoUsuarioId;
    private Integer tipoDocumentoId;
    private String documento;
    private String nombres;
    private String apellidos;
    private String correo;
    private String estado;
    private Integer localidadId;
    private String direccion;
    private String contrasena;
    private String fechaNacimiento;
    private String fechaRegistro;

}
