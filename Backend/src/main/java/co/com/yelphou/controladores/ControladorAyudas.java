/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.yelphou.controladores;

import co.com.yelphou.controladores.dto.AplicarAyudaDto;
import co.com.yelphou.controladores.dto.AyudaDto;
import co.com.yelphou.controladores.dto.RegistrarAyudaDto;
import co.com.yelphou.controladores.dto.SolicitudAyudaDto;
import co.com.yelphou.servicios.ServicioAyudas;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author PERSONAL
 */
@RestController()
@RequestMapping("/ayudas")
public class ControladorAyudas {

    @Autowired
    private ServicioAyudas servicioAyudas;

    @GetMapping(value = "/categorias")
    public ResponseEntity categorias() {
        return new ResponseEntity<>(servicioAyudas.categoriaAyuda(), HttpStatus.OK);
    }

    @GetMapping(value = "/ayudasActivas")
    public ResponseEntity ayudasActivas(@RequestParam Integer localidadId,
            @RequestParam(required = false) Integer categoriaId) {
        return servicioAyudas.ayudasActivas(localidadId, categoriaId);
    }

    @GetMapping(value = "/ayudasUsuario")
    public ResponseEntity ayudasUsuario(@RequestParam Integer usuarioId) {
        return servicioAyudas.ayudasUsuario(usuarioId);
    }

    @GetMapping(value = "/solicitudesUsuario")
    public ResponseEntity solicitudesUsuario(@RequestParam Integer usuarioId) {
        return servicioAyudas.solicitudesUsuario(usuarioId);
    }

    @GetMapping(value = "/solicitudesPorAyuda")
    public ResponseEntity solicitudesPorAyuda(@RequestParam Integer ayudaId) {
        return servicioAyudas.solicitudesPorAyuda(ayudaId);
    }

    @PostMapping(value = "/registrar")
    public ResponseEntity registrar(@RequestBody RegistrarAyudaDto ayuda) {
        return servicioAyudas.registrar(ayuda);
    }

    @PutMapping(value = "/inactivarAyuda")
    public ResponseEntity inactivarAyuda(@RequestBody AyudaDto ayuda) {
        return servicioAyudas.inactivarAyuda(ayuda);
    }

    @PostMapping(value = "/aplicar")
    public ResponseEntity aplicar(@RequestBody AplicarAyudaDto aplicarAyuda) {
        return servicioAyudas.aplicar(aplicarAyuda);
    }

    @PutMapping(value = "/cambiarEstadoSolicitud")
    public ResponseEntity cambiarEstadoSolicitud(@RequestBody SolicitudAyudaDto solicitud) {
        return servicioAyudas.cambiarEstadoSolicitud(solicitud);
    }

}
