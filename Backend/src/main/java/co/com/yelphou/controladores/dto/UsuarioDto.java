/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.yelphou.controladores.dto;

import co.com.yelphou.entidades.Localidad;
import co.com.yelphou.entidades.TipoUsuario;
import java.util.Date;
import lombok.Data;

/**
 *
 * @author PERSONAL
 */
@Data
public class UsuarioDto {

    private Integer id;
    private TipoUsuario tipoUsuario;
    private TipoDocumentoDto tipoDocumento;
    private String documento;
    private String nombres;
    private String apellidos;
    private Date fechaNacimiento;
    private String direccion;
    private Localidad localidad;

}
