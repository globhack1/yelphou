/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.yelphou.controladores.mapper;

import co.com.yelphou.controladores.dto.AplicarAyudaDto;
import co.com.yelphou.controladores.dto.AyudaDto;
import co.com.yelphou.controladores.dto.CategoriaAyudasDto;
import co.com.yelphou.controladores.dto.LocalidadesDto;
import co.com.yelphou.controladores.dto.RegistrarAyudaDto;
import co.com.yelphou.controladores.dto.RegistrarUsuarioDto;
import co.com.yelphou.controladores.dto.SolicitudAyudaDto;
import co.com.yelphou.controladores.dto.TipoDocumentoDto;
import co.com.yelphou.controladores.dto.TipoUsuariosDto;
import co.com.yelphou.controladores.dto.UsuarioDto;
import co.com.yelphou.entidades.Ayuda;
import co.com.yelphou.entidades.CategoriaAyuda;
import co.com.yelphou.entidades.Localidad;
import co.com.yelphou.entidades.SolicitudAyuda;
import co.com.yelphou.entidades.TipoDocumento;
import co.com.yelphou.entidades.TipoUsuario;
import co.com.yelphou.entidades.Usuario;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 *
 * @author Nicolas
 */
@Mapper(componentModel = "spring")
public interface UtilidadesMapper {

    TipoDocumentoDto entityToDto(TipoDocumento tipodocumento);

    List<TipoDocumentoDto> entityToDto(List<TipoDocumento> tipodocumento);

    LocalidadesDto entityToDtoLocalidades(Localidad localidad);

    List<LocalidadesDto> entityToDtoLocalidades(List<Localidad> localidad);

    TipoUsuariosDto entityToDtoTipoUsuario(TipoUsuario tiposuario);

    List<TipoUsuariosDto> entityToDtoTipoUsuario(List<TipoUsuario> tiposuario);

    @Mapping(target = "fechaNacimiento", source = "fechaNacimiento", dateFormat = "yyyy/MM/dd")
    Usuario entityToDto(RegistrarUsuarioDto registrarUsuario);

    UsuarioDto entityToDto(Usuario usuario);

    Ayuda entityToDto(RegistrarAyudaDto registroAyuda);

    CategoriaAyudasDto entityToDtoCategoriaAyuda(CategoriaAyuda categoriaAyuda);

    List<CategoriaAyudasDto> entityToDtoCategoriaAyuda(List<CategoriaAyuda> categoriaAyuda);

    SolicitudAyuda entityToDto(AplicarAyudaDto aplicarAyuda);

    AyudaDto entityToDto(Ayuda ayuda);

    List<AyudaDto> entitiesToDtoAyudas(List<Ayuda> ayudas);

    SolicitudAyudaDto entityToDto(SolicitudAyuda solicitud);

    List<SolicitudAyudaDto> entitiesToDtoSolicitudes(List<SolicitudAyuda> solicitud);

}
