/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.yelphou.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author PERSONAL
 */
@Getter
@Setter
@Entity
@Table(name = "ayuda")
@Cacheable(false)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ayuda.findAll", query = "SELECT a FROM Ayuda a"),
    @NamedQuery(name = "Ayuda.findById", query = "SELECT a FROM Ayuda a WHERE a.id = :id"),
    @NamedQuery(name = "Ayuda.findByCategoriaAyuda", query = "SELECT a FROM Ayuda a WHERE a.categoriaAyuda = :categoriaAyuda"),
    @NamedQuery(name = "Ayuda.findByEstado", query = "SELECT a FROM Ayuda a WHERE a.estado = :estado"),
    @NamedQuery(name = "Ayuda.findByDescripcion", query = "SELECT a FROM Ayuda a WHERE a.descripcion = :descripcion"),
    @NamedQuery(name = "Ayuda.findByFechaRegistro", query = "SELECT a FROM Ayuda a WHERE a.fechaRegistro = :fechaRegistro"),
    @NamedQuery(name = "Ayuda.findByGps", query = "SELECT a FROM Ayuda a WHERE a.gps = :gps"),
    @NamedQuery(name = "Ayuda.findByLocalidadAndCategoria", query = "SELECT a FROM Ayuda a WHERE a.localidadId = :localidadId AND a.categoriaAyudaId = :categoriaId AND a.estado = 'ACTIVA'"),
    @NamedQuery(name = "Ayuda.findByUsuarioId", query = "SELECT a FROM Ayuda a WHERE a.usuarioOferenteId = :usuarioId"),
    @NamedQuery(name = "Ayuda.findByLocalidadId", query = "SELECT a FROM Ayuda a WHERE a.localidadId = :localidadId AND a.estado = 'ACTIVA'")})
public class Ayuda implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "estado")
    private String estado;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "fecha_registro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRegistro;

    @Column(name = "gps")
    private String gps;

    @Basic(optional = false)
    @Column(name = "categoria_ayuda")
    private int categoriaAyudaId;

    @JoinColumn(name = "categoria_ayuda", referencedColumnName = "id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private CategoriaAyuda categoriaAyuda;

    @Column(name = "usuario_oferente")
    private int usuarioOferenteId;

    @JoinColumn(name = "usuario_oferente", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Usuario usuarioOferente;

    @Column(name = "localidad_id")
    private int localidadId;

    @JoinColumn(name = "localidad_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private Localidad localidad;

    @Override
    public String toString() {
        return "co.com.yelphou.entidades.Ayuda[ id=" + id + " ]";
    }

}
