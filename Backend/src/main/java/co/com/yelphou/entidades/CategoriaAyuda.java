/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.yelphou.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author PERSONAL
 */
@Getter
@Setter
@Entity
@Table(name = "categoria_ayuda")
@Cacheable(false)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CategoriaAyuda.findAll", query = "SELECT c FROM CategoriaAyuda c"),
    @NamedQuery(name = "CategoriaAyuda.findById", query = "SELECT c FROM CategoriaAyuda c WHERE c.id = :id"),
    @NamedQuery(name = "CategoriaAyuda.findByDescripcion", query = "SELECT c FROM CategoriaAyuda c WHERE c.descripcion = :descripcion")})
public class CategoriaAyuda implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @Column(name = "descripcion")
    private String descripcion;

    @Override
    public String toString() {
        return "co.com.yelphou.entidades.CategoriaAyuda[ id=" + id + " ]";
    }

}
