/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.yelphou.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author PERSONAL
 */
@Getter
@Setter
@Entity
@Table(name = "solicitud_ayuda")
@Cacheable(false)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SolicitudAyuda.findAll", query = "SELECT s FROM SolicitudAyuda s"),
    @NamedQuery(name = "SolicitudAyuda.findById", query = "SELECT s FROM SolicitudAyuda s WHERE s.id = :id"),
    @NamedQuery(name = "SolicitudAyuda.findByAyudaId", query = "SELECT s FROM SolicitudAyuda s WHERE s.ayudaId = :ayudaId AND s.estado <> 'RECHAZADA'"),
    @NamedQuery(name = "SolicitudAyuda.findByUsuarioReceptor", query = "SELECT s FROM SolicitudAyuda s WHERE s.usuarioReceptor = :usuarioReceptor AND s.estado <> 'RECHAZADA'"),
    @NamedQuery(name = "SolicitudAyuda.findByEstado", query = "SELECT s FROM SolicitudAyuda s WHERE s.estado = :estado"),
    @NamedQuery(name = "SolicitudAyuda.findByDescripcion", query = "SELECT s FROM SolicitudAyuda s WHERE s.descripcion = :descripcion"),
    @NamedQuery(name = "SolicitudAyuda.findByFechaRegistro", query = "SELECT s FROM SolicitudAyuda s WHERE s.fechaRegistro = :fechaRegistro")})
public class SolicitudAyuda implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "estado")
    private String estado;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "fecha_registro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRegistro;

    @Basic(optional = false)
    @Column(name = "ayuda_id")
    private int ayudaId;

    @JoinColumn(name = "ayuda_id", referencedColumnName = "id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Ayuda ayuda;

    @Basic(optional = false)
    @Column(name = "usuario_receptor")
    private int usuarioReceptor;

    @JoinColumn(name = "usuario_receptor", referencedColumnName = "id", insertable = false, updatable = false)
    @OneToOne(optional = false, fetch = FetchType.LAZY)
    private Usuario usuario;

    @Override
    public String toString() {
        return "co.com.yelphou.entidades.SolicitudAyuda[ id=" + id + " ]";
    }

}
