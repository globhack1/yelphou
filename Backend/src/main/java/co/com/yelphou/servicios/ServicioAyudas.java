/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.yelphou.servicios;

import co.com.yelphou.controladores.dto.AplicarAyudaDto;
import co.com.yelphou.controladores.dto.AyudaDto;
import co.com.yelphou.controladores.dto.CategoriaAyudasDto;
import co.com.yelphou.controladores.dto.RegistrarAyudaDto;
import co.com.yelphou.controladores.dto.SolicitudAyudaDto;
import co.com.yelphou.controladores.mapper.UtilidadesMapper;
import co.com.yelphou.entidades.Ayuda;
import co.com.yelphou.entidades.CategoriaAyuda;
import co.com.yelphou.entidades.SolicitudAyuda;
import co.com.yelphou.repositorios.IRepoAyudas;
import co.com.yelphou.repositorios.IRepoCategoriaAyudas;
import co.com.yelphou.repositorios.IRepoSolicitudAyuda;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import utilidades.Constantes;
import utilidades.Mensajes;

/**
 *
 * @author PERSONAL
 */
@Service
public class ServicioAyudas {

    @Autowired
    private UtilidadesMapper mapper;

    @Autowired
    private IRepoAyudas repositorioAyudas;

    @Autowired
    private IRepoCategoriaAyudas repoCategoriaAyudas;

    @Autowired
    private IRepoSolicitudAyuda repoSolicitudAyuda;

    public List<CategoriaAyudasDto> categoriaAyuda() {
        return mapper.entityToDtoCategoriaAyuda((List<CategoriaAyuda>) repoCategoriaAyudas.findAll());
    }

    public ResponseEntity<Object> ayudasActivas(Integer localidadId, Integer categoriaId) {
        if (localidadId != null && categoriaId != null && categoriaId > 0) {
            List<Ayuda> ayudas = repositorioAyudas.findByLocalidadAndCategoria(localidadId,
                    categoriaId);
            return new ResponseEntity<>(mapper.entitiesToDtoAyudas(ayudas), HttpStatus.OK);
        } else if (localidadId != null) {
            List<Ayuda> ayudas = repositorioAyudas.findByLocalidadId(localidadId);
            return new ResponseEntity<>(mapper.entitiesToDtoAyudas(ayudas), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(Mensajes.INFO_INSUFICIENTE, HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<Object> ayudasUsuario(Integer usuarioId) {
        if (usuarioId != null) {
            List<Ayuda> ayudas = repositorioAyudas.findByUsuarioId(usuarioId);
            return new ResponseEntity<>(mapper.entitiesToDtoAyudas(ayudas), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(Mensajes.INFO_INSUFICIENTE, HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<Object> solicitudesUsuario(Integer usuarioId) {
        if (usuarioId != null) {
            List<SolicitudAyuda> solicitudes = repoSolicitudAyuda.findByUsuarioReceptor(usuarioId);
            return new ResponseEntity<>(mapper.entitiesToDtoSolicitudes(solicitudes), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(Mensajes.INFO_INSUFICIENTE, HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<Object> solicitudesPorAyuda(Integer ayudaId) {
        if (ayudaId != null) {
            List<SolicitudAyuda> solicitudes = repoSolicitudAyuda.findByAyudaId(ayudaId);
            return new ResponseEntity<>(mapper.entitiesToDtoSolicitudes(solicitudes), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(Mensajes.INFO_INSUFICIENTE, HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity registrar(RegistrarAyudaDto ayuda) {
        if (validarInformacionRegistro(ayuda)) {
            Ayuda reg = mapper.entityToDto(ayuda);
            reg.setEstado(Constantes.AYUDA_ACTIVA);
            reg.setFechaRegistro(new Date());
            repositorioAyudas.save(reg);
            return new ResponseEntity<>(reg, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(Mensajes.INFO_INSUFICIENTE, HttpStatus.BAD_REQUEST);
        }
    }

    private boolean validarInformacionRegistro(RegistrarAyudaDto ayuda) {
        return ayuda.getCategoriaAyudaId() != null && ayuda.getLocalidadId() != null
                && ayuda.getUsuarioOferenteId() != null
                && ayuda.getDescripcion() != null;
    }

    public ResponseEntity inactivarAyuda(AyudaDto ayuda) {
        if (ayuda.getId() != null) {
            Optional<Ayuda> opt = repositorioAyudas.findById(ayuda.getId());
            if (opt.isPresent()) {
                Ayuda tmp = opt.get();
                tmp.setEstado(Constantes.AYUDA_INACTIVA);
                tmp.setFechaRegistro(new Date());
                repositorioAyudas.save(tmp);
                return new ResponseEntity<>(tmp.getEstado(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
            }
        } else {
            return new ResponseEntity<>(Mensajes.INFO_INSUFICIENTE, HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity aplicar(AplicarAyudaDto aplicarAyuda) {
        if (validarInformacionAplicar(aplicarAyuda)) {
            SolicitudAyuda reg = mapper.entityToDto(aplicarAyuda);
            reg.setEstado(Constantes.SOLICITUD_PENDIENTE);
            reg.setFechaRegistro(new Date());
            repoSolicitudAyuda.save(reg);
            SolicitudAyudaDto resp = mapper.entityToDto(reg);
            return new ResponseEntity<>(resp, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(Mensajes.INFO_INSUFICIENTE, HttpStatus.BAD_REQUEST);
        }
    }

    private boolean validarInformacionAplicar(AplicarAyudaDto aplicarAyuda) {
        return aplicarAyuda.getAyudaId() != null && aplicarAyuda.getUsuarioReceptor() != null
                && aplicarAyuda.getDescripcion() != null;
    }

    public ResponseEntity cambiarEstadoSolicitud(SolicitudAyudaDto solicitud) {
        if (validarCambioEstadoSolicitud(solicitud)) {
            Optional<SolicitudAyuda> opt = repoSolicitudAyuda.findById(solicitud.getId());
            if (opt.isPresent()) {
                SolicitudAyuda tmp = opt.get();
                tmp.setEstado(solicitud.getEstado());
                tmp.setFechaRegistro(new Date());
                repoSolicitudAyuda.save(tmp);
                SolicitudAyudaDto resp = mapper.entityToDto(tmp);
                return new ResponseEntity<>(resp, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
            }
        } else {
            return new ResponseEntity<>(Mensajes.INFO_INSUFICIENTE, HttpStatus.BAD_REQUEST);
        }
    }

    private boolean validarCambioEstadoSolicitud(SolicitudAyudaDto solicitud) {
        if (solicitud.getId() != null) {
            return Constantes.SOLICITUD_APROBADA.equals(solicitud.getEstado())
                    || Constantes.SOLICITUD_RECHAZADA.equals(solicitud.getEstado())
                    || Constantes.SOLICITUD_PENDIENTE.equals(solicitud.getEstado());
        } else {
            return false;
        }
    }

}
