/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.yelphou.servicios;

import co.com.yelphou.controladores.dto.LoginDto;
import co.com.yelphou.controladores.dto.RegistrarUsuarioDto;
import co.com.yelphou.controladores.dto.TipoUsuariosDto;
import co.com.yelphou.controladores.dto.UsuarioDto;
import co.com.yelphou.controladores.mapper.UtilidadesMapper;
import co.com.yelphou.entidades.TipoUsuario;
import co.com.yelphou.entidades.Usuario;
import co.com.yelphou.repositorios.IRepoTipoUsuarios;
import co.com.yelphou.repositorios.IRepoUsuarios;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import utilidades.Constantes;
import utilidades.Mensajes;

/**
 *
 * @author PERSONAL
 */
@Service
public class ServicioUsuarios {

    @Autowired
    private UtilidadesMapper mapper;

    @Autowired
    private IRepoUsuarios repositorioUsuarios;

    @Autowired
    private IRepoTipoUsuarios repoTipoUsuarios;

    public ResponseEntity registrar(RegistrarUsuarioDto usuario) {
        if (validarInformacionRegistro(usuario)) {
            String correo = usuario.getCorreo();

            Usuario tmp = repositorioUsuarios.findByCorreo(correo);
            if (tmp != null) {
                return new ResponseEntity<>(Mensajes.CORREO_EXISTENTE, HttpStatus.OK);
            } else {
                Usuario reg = mapper.entityToDto(usuario);
                reg.setEstado(Constantes.USUARIO_ACTIVO);
                reg.setFechaRegistro(new Date());
                repositorioUsuarios.save(reg);
                UsuarioDto resp = mapper.entityToDto(reg);
                return new ResponseEntity<>(resp, HttpStatus.CREATED);
            }

        } else {
            return new ResponseEntity<>(Mensajes.INFO_INSUFICIENTE, HttpStatus.BAD_REQUEST);
        }
    }

    private boolean validarInformacionRegistro(RegistrarUsuarioDto usuario) {
        return usuario.getTipoUsuarioId() != null && usuario.getTipoDocumentoId() != null
                && usuario.getLocalidadId() != null
                && usuario.getCorreo() != null
                && usuario.getContrasena() != null;
    }

    public List<TipoUsuariosDto> tipoUsuarios() {
        return mapper.entityToDtoTipoUsuario((List<TipoUsuario>) repoTipoUsuarios.findAll());
    }

    public ResponseEntity<Object> login(LoginDto login) {
        if (login.getCorreo() != null && login.getContrasena() != null) {
            Usuario usuario = repositorioUsuarios.findByCorreo(login.getCorreo());
            if (usuario != null && login.getContrasena().equals(usuario.getContrasena())) {
                UsuarioDto tmp = mapper.entityToDto(usuario);
                return new ResponseEntity<>(tmp, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(Mensajes.LOGIN_NO_VALIDO, HttpStatus.FORBIDDEN);
            }
        } else {
            return new ResponseEntity<>(Mensajes.INFO_INSUFICIENTE, HttpStatus.BAD_REQUEST);
        }
    }

}
