/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.yelphou.servicios;

import co.com.yelphou.controladores.dto.LocalidadesDto;
import co.com.yelphou.controladores.dto.TipoDocumentoDto;
import co.com.yelphou.controladores.mapper.UtilidadesMapper;
import co.com.yelphou.entidades.Localidad;
import co.com.yelphou.entidades.TipoDocumento;
import co.com.yelphou.repositorios.IRepoLocalidades;
import co.com.yelphou.repositorios.IRepoTipoDocumento;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Nicolas
 */
@Service
public class ServicioUtilidades {

    @Autowired
    private UtilidadesMapper mapper;

    @Autowired
    private IRepoTipoDocumento repoTipoDocumentos;
    
    @Autowired
    private IRepoLocalidades repoLocalidades;
    

    public List<TipoDocumentoDto> tipoDocumentos() {
        return mapper.entityToDto((List<TipoDocumento>) repoTipoDocumentos.findAll());
    }
    
    public List<LocalidadesDto> localidades() {
        return mapper.entityToDtoLocalidades((List<Localidad>) repoLocalidades.findAll());
    }
}
