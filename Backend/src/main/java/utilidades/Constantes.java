/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilidades;

/**
 *
 * @author PERSONAL
 */
public final class Constantes {

    public final static String USUARIO_ACTIVO = "ACTIVO";
    public final static String AYUDA_ACTIVA = "ACTIVA";
    public final static String AYUDA_INACTIVA = "INACTIVA";
    public final static String SOLICITUD_PENDIENTE = "PENDIENTE";
    public final static String SOLICITUD_APROBADA = "APROBADA";
    public final static String SOLICITUD_RECHAZADA = "RECHAZADA";

    private Constantes() {
    }

}
