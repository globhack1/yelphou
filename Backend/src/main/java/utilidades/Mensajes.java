/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilidades;

/**
 *
 * @author PERSONAL
 */
public final class Mensajes {

    public static final String INFO_INSUFICIENTE = "Información insuficiente";
    public static final String LOGIN_NO_VALIDO = "Correo o contraseña invalida";
    public static final String CORREO_EXISTENTE = "La dirección de correo ya se encuentra registrada en el sistema";

    private Mensajes() {
    }

}
