CREATE DATABASE  IF NOT EXISTS `yelphou` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `yelphou`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: yelphou
-- ------------------------------------------------------
-- Server version	5.6.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ayuda`
--

DROP TABLE IF EXISTS `ayuda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ayuda` (
  `id` int(11) NOT NULL COMMENT 'Identificador ayuda',
  `categoria_ayuda` int(11) NOT NULL COMMENT 'Categoría de la ayuda',
  `usuario_oferente` int(11) NOT NULL COMMENT 'Identificador del usuario que comparte la ayuda',
  `estado` varchar(45) DEFAULT NULL COMMENT 'Estado de la ayuda',
  `descripcion` varchar(200) DEFAULT NULL COMMENT 'Breve descripción de la ayuda',
  `fecha_registro` datetime DEFAULT NULL COMMENT 'Fecha de registro de la ayuda',
  `localidad_id` int(11) DEFAULT NULL COMMENT 'Identificador de la localidad donde se prestará la ayuda',
  `gps` varchar(100) DEFAULT NULL COMMENT 'Ubicación GPS de la ayuda a brindar',
  PRIMARY KEY (`id`),
  KEY `fk_usuario_id_idx` (`usuario_oferente`),
  KEY `fk_localidad_id_idx` (`localidad_id`),
  CONSTRAINT `fk_categoria_ayuda_id` FOREIGN KEY (`id`) REFERENCES `categoria_ayuda` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_ayuda_id` FOREIGN KEY (`usuario_oferente`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_localidad_ayuda_id` FOREIGN KEY (`localidad_id`) REFERENCES `localidad` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Ayudas disponibles en el sistema según su estado y localización';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ayuda`
--

LOCK TABLES `ayuda` WRITE;
/*!40000 ALTER TABLE `ayuda` DISABLE KEYS */;
/*!40000 ALTER TABLE `ayuda` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categoria_ayuda`
--

DROP TABLE IF EXISTS `categoria_ayuda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categoria_ayuda` (
  `id` int(10) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Posibles categorías de las ayudas para donar o recibir';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categoria_ayuda`
--

LOCK TABLES `categoria_ayuda` WRITE;
/*!40000 ALTER TABLE `categoria_ayuda` DISABLE KEYS */;
INSERT INTO `categoria_ayuda` (`id`, `descripcion`) VALUES (1,'Alimentos perecederos'),(2,'Alimentos no perecederos'),(3,'Ropa'),(4,'Dispositivos electrónicos');
/*!40000 ALTER TABLE `categoria_ayuda` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `localidad`
--

DROP TABLE IF EXISTS `localidad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `localidad` (
  `id` int(11) NOT NULL COMMENT 'Identificador localidad',
  `codigo` varchar(10) NOT NULL,
  `nombre` varchar(60) NOT NULL COMMENT 'Nombre localidad',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Ubicación por localidades';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `localidad`
--

LOCK TABLES `localidad` WRITE;
/*!40000 ALTER TABLE `localidad` DISABLE KEYS */;
INSERT INTO `localidad` (`id`, `codigo`, `nombre`) VALUES (1,'01','Usaquén'),(2,'02','Chapinero'),(3,'03','Santa Fe'),(4,'04','San Cristóbal'),(5,'05','Usme'),(6,'06','Tunjuelito'),(7,'07','Bosa'),(8,'08','Kennedy'),(9,'09','Fontibón'),(10,'10','Engativá'),(11,'11','Suba'),(12,'12','Barrios Unidos'),(13,'13','Teusaquillo'),(14,'14','Los Mártires'),(15,'15','Antonio Nariño'),(16,'16','Puente Aranda'),(17,'17','La Candelaria'),(18,'18','Rafael Uribe Uribe'),(19,'19','Ciudad Bolívar'),(20,'20','Sumapaz');
/*!40000 ALTER TABLE `localidad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `solicitud_ayuda`
--

DROP TABLE IF EXISTS `solicitud_ayuda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `solicitud_ayuda` (
  `id` int(11) NOT NULL COMMENT 'Identificador solicitud ayuda',
  `ayuda_id` int(11) NOT NULL COMMENT 'Identificador ayuda a obtener',
  `usuario_receptor` int(11) NOT NULL COMMENT 'Identificador del usuario que solicita la ayuda',
  `estado` varchar(45) DEFAULT NULL COMMENT 'Estado solicitud',
  `descripcion` varchar(200) DEFAULT NULL COMMENT 'Breve descripción de por qué el usuario receptor solicita esta ayuda',
  `fecha_registro` datetime DEFAULT NULL COMMENT 'Fecha de registro de la solicitud',
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_ayuda_id` FOREIGN KEY (`id`) REFERENCES `ayuda` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_receptor_id` FOREIGN KEY (`id`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Solicitudes de usuarios para obtener una ayuda disponible';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `solicitud_ayuda`
--

LOCK TABLES `solicitud_ayuda` WRITE;
/*!40000 ALTER TABLE `solicitud_ayuda` DISABLE KEYS */;
/*!40000 ALTER TABLE `solicitud_ayuda` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_documento`
--

DROP TABLE IF EXISTS `tipo_documento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_documento` (
  `id` int(11) NOT NULL COMMENT 'Identificador tipo documento',
  `codigo` varchar(10) DEFAULT NULL COMMENT 'Abreviatura tipo documento',
  `descripcion` varchar(50) DEFAULT NULL COMMENT 'Descripción tipo documento',
  PRIMARY KEY (`id`),
  UNIQUE KEY `codigo_UNIQUE` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tipos de documentos';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_documento`
--

LOCK TABLES `tipo_documento` WRITE;
/*!40000 ALTER TABLE `tipo_documento` DISABLE KEYS */;
INSERT INTO `tipo_documento` (`id`, `codigo`, `descripcion`) VALUES (1,'CC','Cedula de ciudadanía'),(2,'CE','Cedula de extranjería'),(3,'PAS','Pasaporte');
/*!40000 ALTER TABLE `tipo_documento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_usuario`
--

DROP TABLE IF EXISTS `tipo_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_usuario` (
  `id` int(10) NOT NULL COMMENT 'Identificador tipo usuario',
  `descripcion` varchar(50) NOT NULL COMMENT 'Descripción usuario',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tipo de usuarios en la aplicación';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_usuario`
--

LOCK TABLES `tipo_usuario` WRITE;
/*!40000 ALTER TABLE `tipo_usuario` DISABLE KEYS */;
INSERT INTO `tipo_usuario` (`id`, `descripcion`) VALUES (1,'Oferente'),(2,'Receptor');
/*!40000 ALTER TABLE `tipo_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL COMMENT 'Identificador usuario',
  `tipo_usuario` int(11) NOT NULL COMMENT 'Tipo usuario ',
  `tipo_documento` int(11) DEFAULT NULL COMMENT 'Tipo documento',
  `documento` varchar(45) DEFAULT NULL COMMENT 'Número documento',
  `nombres` varchar(45) DEFAULT NULL COMMENT 'Nombres',
  `apellidos` varchar(45) DEFAULT NULL COMMENT 'Apellidos',
  `correo` varchar(45) DEFAULT NULL COMMENT 'Correo electrónico',
  `estado` varchar(45) DEFAULT NULL COMMENT 'Estado',
  `localidad` int(11) DEFAULT NULL COMMENT 'Localidad residencia',
  `direccion` varchar(45) DEFAULT NULL COMMENT 'Dirección residencia',
  `contrasena` varchar(45) DEFAULT NULL COMMENT 'Contraseña',
  `fecha_nacimiento` date DEFAULT NULL COMMENT 'Fecha nacimiento',
  `fecha_registro` date DEFAULT NULL COMMENT 'Fecha registro en el sistema',
  PRIMARY KEY (`id`),
  KEY `fk_tipo_usuario_id_idx` (`tipo_usuario`),
  KEY `fk_tipo_documento_id_idx` (`tipo_documento`),
  KEY `fk_localidad_id_idx` (`localidad`),
  CONSTRAINT `fk_tipo_usuario_id` FOREIGN KEY (`tipo_usuario`) REFERENCES `tipo_usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tipo_documento_id` FOREIGN KEY (`tipo_documento`) REFERENCES `tipo_documento` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_localidad_id` FOREIGN KEY (`localidad`) REFERENCES `localidad` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Usuarios de la aplicación';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-30 12:10:48
